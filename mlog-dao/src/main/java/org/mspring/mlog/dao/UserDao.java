package org.mspring.mlog.dao;

import org.mspring.mlog.entity.User;
import org.mspring.nbee.orm.dao.IBaseDao;

/**
 *
 * @author Gao Youbo
 * @since 2014-09-10 11:44:46
 */
public interface UserDao extends IBaseDao<User> {

    public User findByUsername(String username);
}
