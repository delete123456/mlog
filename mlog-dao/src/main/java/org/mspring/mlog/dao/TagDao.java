package org.mspring.mlog.dao;

import org.mspring.mlog.entity.Tag;
import org.mspring.nbee.orm.dao.IBaseDao;

import java.util.List;

/**
 * @author Gao Youbo
 * @since 2015-01-02 15:49:28
 */
public interface TagDao extends IBaseDao<Tag> {

    /**
     * 根据名称模糊查找
     *
     * @param name
     * @return
     */
    public List<Tag> listByName(String name);

    /**
     * 列出文章对应的标签
     *
     * @param articleId
     * @return
     */
    public List<Tag> listByArticle(long articleId);

    /**
     * 根据名称查找
     *
     * @param name
     * @return
     */
    public Tag findByName(String name);

}
