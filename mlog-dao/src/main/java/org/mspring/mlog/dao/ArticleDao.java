package org.mspring.mlog.dao;

import org.mspring.nbee.orm.dao.IBaseDao;
import org.mspring.mlog.entity.Article;

/**
 *
 * @author Gao Youbo
 * @since 2014-12-04 17:58:05
 */
public interface ArticleDao extends IBaseDao<Article> {

}
