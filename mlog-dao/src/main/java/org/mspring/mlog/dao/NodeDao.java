package org.mspring.mlog.dao;

import org.mspring.mlog.entity.Node;
import org.mspring.nbee.orm.dao.IBaseDao;

import java.util.List;

/**
 * @author Gao Youbo
 * @since 2014-09-10 11:44:46
 */
public interface NodeDao extends IBaseDao<Node> {

    /**
     * 根据角色查找角色对应的树形菜单
     *
     * @param roleId
     * @return
     */
    public List<Node> listByRole(long roleId);

    /**
     * 加载树形菜单，Type为NodeType.TREE_FOLDER, NodeType.TREE_ITEM
     *
     * @param userId
     * @return
     */
    public List<Node> listTreeByUser(long userId);

    /**
     * 加载tab页签
     *
     * @param parent 父节点编号
     * @param userId 用户编号
     * @return
     */
    public List<Node> listTabByUser(String parent, long userId);

    /**
     * 加载树形菜单节点
     *
     * @param parent 父节点编号
     * @param userId 用户编号
     * @return
     */
    public Node getOpenTab(String parent, long userId);

    /**
     * 删除所有
     *
     * @return
     */
    public boolean deleteAll();
}
