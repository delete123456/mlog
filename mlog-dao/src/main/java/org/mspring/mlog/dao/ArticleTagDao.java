package org.mspring.mlog.dao;

import org.mspring.mlog.entity.ArticleTag;
import org.mspring.nbee.orm.dao.IBaseDao;

/**
 * @author Gao Youbo
 * @since 2015-01-03 13:41:58
 */
public interface ArticleTagDao extends IBaseDao<ArticleTag> {
    /**
     * 删除文章对应的标签管理
     *
     * @param articleId
     * @return
     */
    public boolean deleteByArticle(long articleId);

}
