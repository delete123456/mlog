package org.mspring.mlog.dao;

import java.util.List;
import org.mspring.mlog.entity.Role;
import org.mspring.nbee.orm.dao.IBaseDao;

/**
 *
 * @author Gao Youbo
 * @since 2014-09-10 11:44:46
 */
public interface RoleDao extends IBaseDao<Role> {

    /**
     * 根据用户编号查询用户对应角色
     *
     * @param userId
     * @return
     */
    public List<Role> listByUser(long userId);
}
