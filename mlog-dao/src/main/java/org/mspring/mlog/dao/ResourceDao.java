package org.mspring.mlog.dao;

import java.util.List;
import org.mspring.mlog.entity.Resource;
import org.mspring.nbee.orm.dao.IBaseDao;

/**
 *
 * @author Gao Youbo
 * @since 2014-09-10 11:44:46
 */
public interface ResourceDao extends IBaseDao<Resource> {

    /**
     * 根据角色查询角色对应资源
     *
     * @param roleId
     * @return
     */
    public List<Resource> listByRole(long roleId);
}
