package org.mspring.mlog.dao;

import org.mspring.mlog.entity.Category;
import org.mspring.nbee.orm.dao.IBaseDao;

/**
 * @author Gao Youbo
 * @since 2014-12-05 11:50:15
 */
public interface CategoryDao extends IBaseDao<Category> {

}
