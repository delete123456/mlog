package org.mspring.mlog.dao;

import org.mspring.mlog.entity.RoleNode;
import org.mspring.nbee.orm.dao.IBaseDao;

/**
 * @author Gao Youbo
 * @since 2015-01-02 14:26
 */
public interface RoleNodeDao extends IBaseDao<RoleNode> {

    /**
     * 删除所有
     *
     * @return
     */
    public boolean deleteAll();
}
