package org.mspring.mlog.dao;

import org.mspring.nbee.orm.dao.IBaseDao;
import org.mspring.mlog.entity.RoleResource;

/**
 *
 * @author Gao Youbo
 * @since 2014-09-10 11:44:46
 */
public interface RoleResourceDao extends IBaseDao<RoleResource> {

}
