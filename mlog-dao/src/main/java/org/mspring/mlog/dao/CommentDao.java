package org.mspring.mlog.dao;

import org.mspring.mlog.entity.Comment;
import org.mspring.nbee.orm.dao.IBaseDao;

/**
 *
 * @author Gao Youbo
 * @since 2015-01-03 14:38:16
 */
public interface CommentDao extends IBaseDao<Comment> {

}
