package org.mspring.mlog.dao;

import org.mspring.mlog.entity.Config;
import org.mspring.nbee.orm.dao.IBaseDao;

/**
 * @author Gao Youbo
 * @since 2015-01-03 18:07:47
 */
public interface ConfigDao extends IBaseDao<Config> {

    /**
     * 根据分组和名称查找
     *
     * @param groupName
     * @param name
     * @return
     */
    public Config findByName(String groupName, String name);

}
