
import java.sql.Connection;
import java.sql.DriverManager;
import org.mspring.nbee.codegen.CreateCode;

/**
 *
 * @author Gao Youbo
 * @since 2014-09-09 11:20:51
 */
public class CodeGen {

    public static void main(String[] args) throws Exception {
        try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mlog17", "root", "gaoyoubo")) {
            CreateCode.createDaoCode(con);
        }
    }
}
