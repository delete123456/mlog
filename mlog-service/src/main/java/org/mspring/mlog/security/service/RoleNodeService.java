package org.mspring.mlog.security.service;

import org.mspring.mlog.dao.RoleNodeDao;
import org.mspring.mlog.entity.RoleNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Gao Youbo
 * @since 2015-01-02 14:25
 */
@Service
public class RoleNodeService {
    private RoleNodeDao roleNodeDao;

    public RoleNode findById(long id) {
        return roleNodeDao.findById(id);
    }

    @Transactional
    public boolean insert(RoleNode roleNode) {
        return roleNodeDao.insert(roleNode);
    }


    @Transactional
    public boolean update(RoleNode roleNode) {
        return roleNodeDao.update(roleNode);
    }

    @Transactional
    public boolean deleteById(long id) {
        return roleNodeDao.deleteById(id);
    }

    /**
     * 删除所有
     *
     * @return
     */
    @Transactional
    public boolean deleteAll() {
        return roleNodeDao.deleteAll();
    }

    @Autowired
    public void setRoleNodeDao(RoleNodeDao roleNodeDao) {
        this.roleNodeDao = roleNodeDao;
    }
}
