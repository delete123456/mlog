package org.mspring.mlog.security.service;

import java.util.List;
import org.mspring.mlog.dao.RoleResourceDao;
import org.mspring.mlog.entity.RoleResource;
import org.mspring.nbee.orm.dao.sql.builder.part.condition.SqlCondition;
import org.mspring.nbee.orm.dao.sql.pager.PageResult;
import org.mspring.nbee.orm.dao.sql.pager.Pager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Gao Youbo
 * @since 2014-09-10 12:44:49
 */
@Service
public class RoleResourceService {

    private static final Logger log = LoggerFactory.getLogger(RoleResourceService.class);

    private RoleResourceDao roleResourceDao;

    public RoleResource findById(long id) {
        return roleResourceDao.findById(id);
    }

    public PageResult<RoleResource> list(List<SqlCondition> conditions, Pager pager) {
        return roleResourceDao.listPage(conditions, pager);
    }

    @Transactional
    public boolean update(RoleResource roleResource) {
        return roleResourceDao.update(roleResource);
    }

    @Transactional
    public boolean deleteById(long id) {
        return roleResourceDao.deleteById(id);
    }

    @Autowired
    public void setRoleResourceDao(RoleResourceDao roleResourceDao) {
        this.roleResourceDao = roleResourceDao;
    }

}