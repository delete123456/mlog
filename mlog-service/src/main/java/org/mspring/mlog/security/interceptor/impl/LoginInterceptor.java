package org.mspring.mlog.security.interceptor.impl;

import org.mspring.mlog.security.interceptor.SecurityInterceptor;
import org.springframework.security.web.FilterInvocation;

/**
 * 登录拦截
 *
 * @author Gao Youbo
 * @since 2014-09-10 17:10:36
 */
public class LoginInterceptor implements SecurityInterceptor {

    @Override
    public boolean handle(FilterInvocation invocation) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
