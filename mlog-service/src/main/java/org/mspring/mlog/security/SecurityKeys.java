package org.mspring.mlog.security;

/**
 *
 * @author Gao Youbo
 * @since 2013年11月23日
 */
public class SecurityKeys {

    public static final String CURRENT_USER = "_CURRENT_USER_";

    public static final String SESSION_VALIDATE_CODE = "_VALIDATE_CODE__";

    public static final String IS_REMEMBER_USER_COOKIE = "MLOG__IS_REMEMBER_USER_COOKIE__";

    public static final String USERNAME_COOKIE = "MLOG__USERNAME_COOKIE__";

    public static final String PASSWORD_COOKIE = "MLOG__PASSWORD_COOKIE__";

}
