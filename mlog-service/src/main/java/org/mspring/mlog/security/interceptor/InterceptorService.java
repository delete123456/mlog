package org.mspring.mlog.security.interceptor;

import java.io.IOException;
import java.util.List;
import javax.servlet.Filter;
import javax.servlet.ServletException;
import org.mspring.nbee.common.utils.CollectionUtils;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;

/**
 *
 * @author Gao Youbo
 * @since 2014-09-10 16:21:35
 */
public class InterceptorService extends FilterSecurityInterceptor implements Filter {

    private List<SecurityInterceptor> interceptors;

    @Override
    public void invoke(FilterInvocation fi) throws IOException, ServletException {
        if (!CollectionUtils.isEmpty(interceptors)) {
            for (SecurityInterceptor interceptor : interceptors) {
                interceptor.handle(fi);
            }
        }
        super.invoke(fi);
    }

    public void setInterceptors(List<SecurityInterceptor> interceptors) {
        this.interceptors = interceptors;
    }

}
