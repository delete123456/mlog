package org.mspring.mlog.security.user;

import java.util.Collection;
import org.mspring.mlog.entity.User;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author Gao Youbo
 * @since 2014-09-10 17:46:14
 */
public class UserDetailsImpl extends org.springframework.security.core.userdetails.User {

    /**
     *
     * @param username
     * @param password
     * @param authorities
     */
    @Deprecated
    public UserDetailsImpl(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        this(username, password, true, true, true, true, authorities);
    }

    /**
     * @param username
     * @param password
     * @param enabled
     * @param accountNonExpired
     * @param credentialsNonExpired
     * @param accountNonLocked
     * @param authorities
     */
    @Deprecated
    public UserDetailsImpl(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);

    }

    public UserDetailsImpl(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities, User userEntity) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        this.userEntity = userEntity;
    }

    /**
     * 该对象为数据库实体类对象
     */
    private User userEntity;

    public User getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(User userEntity) {
        this.userEntity = userEntity;
    }
}
