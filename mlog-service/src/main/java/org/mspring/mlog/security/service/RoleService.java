package org.mspring.mlog.security.service;

import java.util.List;
import org.mspring.mlog.dao.RoleDao;
import org.mspring.mlog.entity.Role;
import org.mspring.nbee.orm.dao.sql.builder.part.condition.SqlCondition;
import org.mspring.nbee.orm.dao.sql.pager.PageResult;
import org.mspring.nbee.orm.dao.sql.pager.Pager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Gao Youbo
 * @since 2014-09-10 12:44:48
 */
@Service
public class RoleService {

    private static final Logger log = LoggerFactory.getLogger(RoleService.class);

    private RoleDao roleDao;

    public Role findById(long id) {
        return roleDao.findById(id);
    }

    public PageResult<Role> list(List<SqlCondition> conditions, Pager pager) {
        return roleDao.listPage(conditions, pager);
    }

    /**
     * 根据用户编号查询用户对应角色
     *
     * @param userId
     * @return
     */
    public List<Role> listByUser(long userId) {
        return roleDao.listByUser(userId);
    }

    @Transactional
    public boolean update(Role role) {
        return roleDao.update(role);
    }

    @Transactional
    public boolean deleteById(long id) {
        return roleDao.deleteById(id);
    }

    @Autowired
    public void setRoleDao(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

}
