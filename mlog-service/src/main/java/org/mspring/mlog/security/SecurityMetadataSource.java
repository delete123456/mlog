package org.mspring.mlog.security;

import org.mspring.mlog.entity.Node;
import org.mspring.mlog.entity.Resource;
import org.mspring.mlog.security.service.NodeService;
import org.mspring.mlog.security.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * @author Gao Youbo
 * @since 2013-1-11
 */
@Component("securityMetadataSource")
public class SecurityMetadataSource implements FilterInvocationSecurityMetadataSource {

    private Map<String, Collection<ConfigAttribute>> resourceMap = null;

    @Autowired
    private ResourceService resourceService;
    @Autowired
    private NodeService nodeService;

    /**
     * 重新加载资源
     */
    public void reloadResourceDefine() {
        resourceMap = null;
        // loadResourceDefine();
    }

    /**
     * 加载所有资源与权限的关系
     */
    public void loadResourceDefine() {
        if (resourceMap == null) {
            resourceMap = new HashMap<>();
            List<Resource> resources = resourceService.listAll();
            for (Resource resource : resources) {
                Collection<ConfigAttribute> configAttributes = new ArrayList<>();
                ConfigAttribute configAttribute = new SecurityConfig("resource_" + resource.getId());
                configAttributes.add(configAttribute);
                resourceMap.put(resource.getUrl(), configAttributes);
            }

            List<Node> nodes = nodeService.listAll();
            Iterator<Node> it = nodes.iterator();
            while (it.hasNext()) {
                Node node = it.next();
                if (org.apache.commons.lang3.StringUtils.isNotBlank(node.getUrl())) {
                    Collection<ConfigAttribute> configAttributes = new ArrayList<>();
                    ConfigAttribute configAttribute = new SecurityConfig("node_" + node.getId());
                    configAttributes.add(configAttribute);
                    String pattern = node.getUrl() + "/**";
                    resourceMap.put(pattern, configAttributes);
                }
            }
        }
    }

    /**
     * 返回所请求资源所需要的权限
     */
    @Override
    public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
        loadResourceDefine();

        String url = ((FilterInvocation) object).getRequestUrl();
        int firstQuestionMarkIndex = url.indexOf("?");
        if (firstQuestionMarkIndex != -1) {
            url = url.substring(0, firstQuestionMarkIndex);
        }
        HttpServletRequest request = ((FilterInvocation) object).getRequest();

        Iterator<String> ite = resourceMap.keySet().iterator();
        while (ite.hasNext()) {
            String resURL = ite.next();
            RequestMatcher urlMatcher = new AntPathRequestMatcher(resURL);
            if (urlMatcher.matches(request)) {
                return resourceMap.get(resURL);
            }
        }
        return null;
    }

    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {

        return null;
    }

    @Override
    public boolean supports(Class<?> clazz) {

        return true;
    }
}
