package org.mspring.mlog.security.service;

import org.mspring.mlog.dao.NodeDao;
import org.mspring.mlog.entity.Node;
import org.mspring.nbee.orm.dao.sql.builder.part.condition.SqlCondition;
import org.mspring.nbee.orm.dao.sql.pager.PageResult;
import org.mspring.nbee.orm.dao.sql.pager.Pager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Gao Youbo
 * @since 2014-09-10 12:44:49
 */
@Service
public class NodeService {

    private NodeDao nodeDao;

    public Node findById(String id) {
        return nodeDao.findById(id);
    }

    public PageResult<Node> list(List<SqlCondition> conditions, Pager pager) {
        return nodeDao.listPage(conditions, pager);
    }

    /**
     * 根据角色查找角色对应的树形菜单
     *
     * @param roleId
     * @return
     */
    public List<Node> listByRole(long roleId) {
        return nodeDao.listByRole(roleId);
    }

    /**
     * 加载树形菜单，Type为NodeType.TREE_FOLDER, NodeType.TREE_ITEM
     *
     * @param userId
     * @return
     */
    public List<Node> listTreeByUser(long userId) {
        return nodeDao.listTreeByUser(userId);
    }

    /**
     * 加载tab页签
     *
     * @param parent 父节点编号
     * @param userId 用户编号
     * @return
     */
    public List<Node> listTabByUser(String parent, long userId) {
        return nodeDao.listTabByUser(parent, userId);
    }

    /**
     * 加载树形菜单节点
     *
     * @param parent 父节点编号
     * @param userId 用户编号
     * @return
     */
    public Node getOpenTab(String parent, long userId) {
        return nodeDao.getOpenTab(parent, userId);
    }

    public List<Node> listAll() {
        return nodeDao.listAll();
    }


    @Transactional
    public boolean insert(Node node) {
        return nodeDao.insert(node);
    }

    @Transactional
    public boolean update(Node node) {
        return nodeDao.update(node);
    }

    @Transactional
    public boolean deleteById(long id) {
        return nodeDao.deleteById(id);
    }

    /**
     * 删除所有
     *
     * @return
     */
    @Transactional
    public boolean deleteAll() {
        return nodeDao.deleteAll();
    }

    @Autowired
    public void setNodeDao(NodeDao nodeDao) {
        this.nodeDao = nodeDao;
    }

}
