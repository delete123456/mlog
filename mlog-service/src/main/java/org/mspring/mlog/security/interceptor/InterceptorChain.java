package org.mspring.mlog.security.interceptor;

import java.util.ArrayList;
import java.util.List;
import org.mspring.nbee.common.utils.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.FilterInvocation;

/**
 *
 * @author Gao Youbo
 * @since 2014-09-10 16:28:58
 */
public class InterceptorChain {

    private static final Logger log = LoggerFactory.getLogger(InterceptorChain.class);

    protected final List<SecurityInterceptor> interceptors = new ArrayList<>();

    public InterceptorChain addFilter(SecurityInterceptor interceptor) {
        interceptors.add(interceptor);
        return this;
    }

    public void doFilter(FilterInvocation invocation) throws Exception {
        if (CollectionUtils.isEmpty(interceptors)) {
            log.warn("防火墙:过滤器列表为空");
            return;
        }
        for (SecurityInterceptor interceptor : interceptors) {
            interceptor.handle(invocation);
        }
    }
}
