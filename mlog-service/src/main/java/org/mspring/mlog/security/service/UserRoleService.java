package org.mspring.mlog.security.service;

import java.util.List;
import org.mspring.mlog.dao.UserRoleDao;
import org.mspring.mlog.entity.UserRole;
import org.mspring.nbee.orm.dao.sql.builder.part.condition.SqlCondition;
import org.mspring.nbee.orm.dao.sql.pager.PageResult;
import org.mspring.nbee.orm.dao.sql.pager.Pager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Gao Youbo
 * @since 2014-09-10 12:44:49
 */
@Service
public class UserRoleService {

    private static final Logger log = LoggerFactory.getLogger(UserRoleService.class);

    private UserRoleDao userRoleDao;

    public UserRole findById(long id) {
        return userRoleDao.findById(id);
    }

    public PageResult<UserRole> list(List<SqlCondition> conditions, Pager pager) {
        return userRoleDao.listPage(conditions, pager);
    }

    @Transactional
    public boolean update(UserRole userRole) {
        return userRoleDao.update(userRole);
    }

    @Transactional
    public boolean deleteById(long id) {
        return userRoleDao.deleteById(id);
    }

    @Autowired
    public void setUserRoleDao(UserRoleDao userRoleDao) {
        this.userRoleDao = userRoleDao;
    }

}