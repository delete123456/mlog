package org.mspring.mlog.security.interceptor;

import org.springframework.security.web.FilterInvocation;

/**
 *
 * @author Gao Youbo
 * @since 2014-09-10 16:23:53
 */
public interface SecurityInterceptor {

    boolean handle(FilterInvocation invocation);
}
