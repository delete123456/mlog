package org.mspring.mlog.security.service;

import java.util.List;
import org.mspring.mlog.dao.ResourceDao;
import org.mspring.mlog.entity.Resource;
import org.mspring.nbee.orm.dao.sql.builder.part.condition.SqlCondition;
import org.mspring.nbee.orm.dao.sql.pager.PageResult;
import org.mspring.nbee.orm.dao.sql.pager.Pager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Gao Youbo
 * @since 2014-09-10 12:44:48
 */
@Service
public class ResourceService {

    private static final Logger log = LoggerFactory.getLogger(ResourceService.class);

    private ResourceDao resourceDao;

    public Resource findById(long id) {
        return resourceDao.findById(id);
    }

    public PageResult<Resource> list(List<SqlCondition> conditions, Pager pager) {
        return resourceDao.listPage(conditions, pager);
    }

    /**
     * 根据角色查询角色对应资源
     *
     * @param roleId
     * @return
     */
    public List<Resource> listByRole(long roleId) {
        return resourceDao.listByRole(roleId);
    }

    public List<Resource> listAll() {
        return resourceDao.listAll();
    }

    @Transactional
    public boolean update(Resource resource) {
        return resourceDao.update(resource);
    }

    @Transactional
    public boolean deleteById(long id) {
        return resourceDao.deleteById(id);
    }

    @Autowired
    public void setResourceDao(ResourceDao resourceDao) {
        this.resourceDao = resourceDao;
    }

}
