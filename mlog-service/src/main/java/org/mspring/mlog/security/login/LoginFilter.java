package org.mspring.mlog.security.login;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.mspring.mlog.entity.User;
import org.mspring.mlog.security.SecurityKeys;
import org.mspring.mlog.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 *
 * @author Gao Youbo
 * @since 2014-09-10 17:25:42
 */
public class LoginFilter extends UsernamePasswordAuthenticationFilter {

    public static final String VALIDATE_CODE_PARAM = "validateCode";
    public static final String USERNAME_PARAM = "username";
    public static final String PASSWORD_PARAM = "password";

    @Autowired
    private UserService userService;

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        if (!request.getMethod().equals("POST")) {
            throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
        }

        // 验证码
        checkValidateCode(request);

        // 用户验证
        String username = obtainUsername(request);
        String password = obtainPassword(request);
        User loginUser = userService.login(username, password);
        if (loginUser == null) {
            throw new AuthenticationServiceException("登录失败，用户名或密码错误");
        }

        UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(username, password);
        // 允许子类设置详细属性
        setDetails(request, authRequest);

        Authentication auth = this.getAuthenticationManager().authenticate(authRequest);

        // 保存登录相关信息
        request.getSession().setAttribute(SecurityKeys.CURRENT_USER, loginUser);
        return auth;
    }

    /**
     * 验证码验证
     *
     * @param request
     */
    protected void checkValidateCode(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String sessionValidateCode = obtainSessionValidateCode(session);
        session.setAttribute(SecurityKeys.SESSION_VALIDATE_CODE, null); //让上一次的验证码失效
        String validateCodeParameter = obtainValidateCodeParameter(request);
        if (org.apache.commons.lang3.StringUtils.isEmpty(validateCodeParameter) || !sessionValidateCode.equalsIgnoreCase(validateCodeParameter)) {
            throw new AuthenticationServiceException("登录失败，验证码输入错误");
        }
    }

    /**
     * 获取表单提交的验证码
     *
     * @param request
     * @return
     */
    private String obtainValidateCodeParameter(HttpServletRequest request) {
        Object obj = request.getParameter(VALIDATE_CODE_PARAM);
        return null == obj ? "" : obj.toString();
    }

    /**
     * 获取Session中保存的验证码
     *
     * @param session
     * @return
     */
    protected String obtainSessionValidateCode(HttpSession session) {
        Object obj = session.getAttribute(SecurityKeys.SESSION_VALIDATE_CODE);
        return null == obj ? "" : obj.toString();
    }

    /**
     * 获取表单提交的用户名
     *
     * @param request
     * @return
     */
    @Override
    protected String obtainUsername(HttpServletRequest request) {
        Object obj = request.getParameter(USERNAME_PARAM);
        return null == obj ? "" : obj.toString();
    }

    /**
     * 获取表单提交的密码
     *
     * @param request
     * @return
     */
    @Override
    protected String obtainPassword(HttpServletRequest request) {
        Object obj = request.getParameter(PASSWORD_PARAM);
        return null == obj ? "" : obj.toString();
    }
}
