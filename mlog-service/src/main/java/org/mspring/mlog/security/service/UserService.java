package org.mspring.mlog.security.service;

import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.mspring.mlog.dao.UserDao;
import org.mspring.mlog.entity.User;
import org.mspring.nbee.orm.dao.sql.builder.part.condition.SqlCondition;
import org.mspring.nbee.orm.dao.sql.pager.PageResult;
import org.mspring.nbee.orm.dao.sql.pager.Pager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Gao Youbo
 * @since 2014-09-10 12:44:49
 */
@Service
public class UserService {

    private static final Logger log = LoggerFactory.getLogger(UserService.class);

    private UserDao userDao;

    public User findById(long id) {
        return userDao.findById(id);
    }

    public User findByUsername(String username) {
        if (StringUtils.isBlank(username)) {
            return null;
        }
        return userDao.findByUsername(username);
    }

    public User login(String username, String password) {
        if (StringUtils.isAnyBlank(username, password)) {
            return null;
        }
        User user = findByUsername(username);
        if (user == null) {
            return null;
        }
        if (StringUtils.equals(password, password)) {
            return user;
        }
        return null;
    }

    public PageResult<User> list(List<SqlCondition> conditions, Pager pager) {
        return userDao.listPage(conditions, pager);
    }

    @Transactional
    public boolean update(User user) {
        return userDao.update(user);
    }

    @Transactional
    public boolean deleteById(long id) {
        return userDao.deleteById(id);
    }

    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

}
