package org.mspring.mlog;

import org.mspring.mlog.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Gao Youbo
 * @since 2015-01-04 14:23
 */
@Component
public class ServiceContext {
    private ArticleService articleService;
    private ArticleTagService articleTagService;
    private CategoryService categoryService;
    private CommentService commentService;
    private ConfigService configService;
    private TagService tagService;

    public ArticleService getArticleService() {
        return articleService;
    }

    @Autowired
    public void setArticleService(ArticleService articleService) {
        this.articleService = articleService;
    }

    public ArticleTagService getArticleTagService() {
        return articleTagService;
    }

    @Autowired
    public void setArticleTagService(ArticleTagService articleTagService) {
        this.articleTagService = articleTagService;
    }

    public CategoryService getCategoryService() {
        return categoryService;
    }

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public CommentService getCommentService() {
        return commentService;
    }

    @Autowired
    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }

    public ConfigService getConfigService() {
        return configService;
    }

    @Autowired
    public void setConfigService(ConfigService configService) {
        this.configService = configService;
    }

    public TagService getTagService() {
        return tagService;
    }

    @Autowired
    public void setTagService(TagService tagService) {
        this.tagService = tagService;
    }
}
