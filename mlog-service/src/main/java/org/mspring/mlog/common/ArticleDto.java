package org.mspring.mlog.common;

import org.mspring.mlog.ServiceContext;
import org.mspring.mlog.entity.Article;
import org.mspring.mlog.entity.Category;
import org.mspring.mlog.entity.Tag;
import org.mspring.nbee.common.utils.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Gao Youbo
 * @since 2015-01-04 14:17
 */
public class ArticleDto {
    private Long id;
    private Category category;
    private String title;
    private String content;
    private List<Tag> tagList;
    private List<ArticleImage> imageList;
    private Date createTime;
    private long createUser;
    private String createUserName;
    private Date updateTime;
    private long updateUser;
    private String updateUserName;

    public static ArticleDto fromArticle(Article article, ServiceContext serviceContext) {
        if (article == null) {
            return null;
        }
        Category category = serviceContext.getCategoryService().findById(article.getCategory());
        List<Tag> tagList = serviceContext.getTagService().listByArticle(article.getId());
        ArticleDto dto = new ArticleDto();
        dto.setId(article.getId());
        dto.setCategory(category);
        dto.setTitle(article.getTitle());
        dto.setContent(article.getContent());
        dto.setTagList(tagList);
        dto.setImageList(ArticleImage.fromJson(article.getImageList()));
        dto.setCreateTime(article.getCreateTime());
        dto.setCreateUser(article.getCreateUser());
        dto.setCreateUserName(article.getCreateUserName());
        dto.setUpdateTime(article.getUpdateTime());
        dto.setUpdateUser(article.getUpdateUser());
        dto.setUpdateUserName(article.getUpdateUserName());
        return dto;
    }

    public static List<ArticleDto> fromArticles(List<Article> articles, ServiceContext serviceContext) {
        List<ArticleDto> ret = new ArrayList<>();
        if (CollectionUtils.isEmpty(articles)) {
            return ret;
        }
        for (Article article : articles) {
            ret.add(fromArticle(article, serviceContext));
        }
        return ret;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }

    public List<ArticleImage> getImageList() {
        return imageList;
    }

    public void setImageList(List<ArticleImage> imageList) {
        this.imageList = imageList;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(long createUser) {
        this.createUser = createUser;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public long getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(long updateUser) {
        this.updateUser = updateUser;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }


}
