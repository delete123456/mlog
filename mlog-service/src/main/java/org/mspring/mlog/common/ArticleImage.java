package org.mspring.mlog.common;

import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * [{"url":"http://mlog-test.qiniudn.com/076e3caed758a1c18c91a0e9cae3368f","key":"076e3caed758a1c18c91a0e9cae3368f","hash":"FvX4rSaBmkcTGNJGMfpQVQNnEqh-"},{"url":"http://mlog-test.qiniudn.com/ba45c8f60456a672e003a875e469d0eb","key":"ba45c8f60456a672e003a875e469d0eb","hash":"FjBCDRqa-yvLYDNYElaa9ENaWc4X"},{"url":"http://mlog-test.qiniudn.com/bdf3bf1da3405725be763540d6601144","key":"bdf3bf1da3405725be763540d6601144","hash":"FtmX4cN-3AWth9A2A-Mq1JXuLPzh"},{"url":"http://mlog-test.qiniudn.com/5a44c7ba5bbe4ec867233d67e4806848","key":"5a44c7ba5bbe4ec867233d67e4806848","hash":"FjsVvoSv8gsyKpPAuaqmLiWtM7S0"},{"url":"http://mlog-test.qiniudn.com/2b04df3ecc1d94afddff082d139c6f15","key":"2b04df3ecc1d94afddff082d139c6f15","hash":"Fpw9yx-RhaMU6iXVGu07WIGzL0IM"},{"url":"http://mlog-test.qiniudn.com/8969288f4245120e7c3870287cce0ff3","key":"8969288f4245120e7c3870287cce0ff3","hash":"FhtGBbDiDOzPkaonjRDoH61k4k4n"},{"url":"http://mlog-test.qiniudn.com/9d377b10ce778c4938b3c7e2c63a229a","key":"9d377b10ce778c4938b3c7e2c63a229a","hash":"Ft976dxPRnGHeDrKaMfOmOTfIXLQ"},{"url":"http://mlog-test.qiniudn.com/fafa5efeaf3cbe3b23b2748d13e629a1","key":"fafa5efeaf3cbe3b23b2748d13e629a1","hash":"FlTC8aHrbxLWgaXHB4QhpVAM7gKt"}]
 *
 * @author Gao Youbo
 * @since 2014-12-31 18:29
 */
public class ArticleImage {
    private static Logger LOG = LoggerFactory.getLogger(ArticleImage.class);

    private String key;
    private String hash;
    private String url;

    public static List<ArticleImage> fromJson(String str) {
        List<ArticleImage> images = new ArrayList<>();
        if (StringUtils.isBlank(str)) {
            return images;
        }
        try {
            images = JSON.parseArray(str, ArticleImage.class);
        } catch (Exception e) {
            LOG.error("图片格式不正确", e);
        }
        return images;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
