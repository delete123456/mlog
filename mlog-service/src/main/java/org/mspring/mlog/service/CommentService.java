package org.mspring.mlog.service;

import org.mspring.mlog.dao.CommentDao;
import org.mspring.mlog.entity.Comment;
import org.mspring.mlog.entity.em.CommentStatus;
import org.mspring.nbee.orm.dao.sql.builder.part.condition.SqlCondition;
import org.mspring.nbee.orm.dao.sql.pager.PageResult;
import org.mspring.nbee.orm.dao.sql.pager.Pager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Gao Youbo
 * @since 2015-01-03 14:38:26
 */
@Service
public class CommentService {

    private static final Logger log = LoggerFactory.getLogger(CommentService.class);

    private CommentDao commentDao;

    public Comment findById(long id) {
        return commentDao.findById(id);
    }

    public PageResult<Comment> list(List<SqlCondition> conditions, Pager pager) {
        return commentDao.listPage(conditions, pager);
    }

    @Transactional
    public boolean insert(Comment comment) {
        return commentDao.insert(comment);
    }

    @Transactional
    public boolean update(Comment comment) {
        return commentDao.update(comment);
    }

    @Transactional
    public boolean deleteById(long id) {
        Comment comment = findById(id);
        if (comment == null) {
            return true;
        }
        comment.setStatus(CommentStatus.DELEtED);
        return commentDao.update(comment);
    }

    @Autowired
    public void setCommentDao(CommentDao commentDao) {
        this.commentDao = commentDao;
    }

}