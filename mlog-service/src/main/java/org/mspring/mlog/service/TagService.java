package org.mspring.mlog.service;

import org.apache.commons.lang3.StringUtils;
import org.mspring.mlog.dao.TagDao;
import org.mspring.mlog.entity.Tag;
import org.mspring.nbee.orm.dao.sql.builder.part.condition.SqlCondition;
import org.mspring.nbee.orm.dao.sql.pager.PageResult;
import org.mspring.nbee.orm.dao.sql.pager.Pager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Gao Youbo
 * @since 2015-01-02 15:51:24
 */
@Service
public class TagService {

    private static final Logger log = LoggerFactory.getLogger(TagService.class);

    private TagDao tagDao;

    public Tag findById(long id) {
        return tagDao.findById(id);
    }

    public PageResult<Tag> list(List<SqlCondition> conditions, Pager pager) {
        return tagDao.listPage(conditions, pager);
    }

    /**
     * 根据名称模糊查找
     *
     * @param name
     * @return
     */
    public List<Tag> listByName(String name) {
        return tagDao.listByName(name);
    }


    /**
     * 列出文章对应的标签
     *
     * @param articleId
     * @return
     */
    public List<Tag> listByArticle(long articleId) {
        return tagDao.listByArticle(articleId);
    }

    /**
     * 根据名称查找
     *
     * @param name
     * @return
     */
    public Tag findByName(String name) {
        if (StringUtils.isBlank(name)) {
            return null;
        }
        return tagDao.findByName(name);
    }

    @Transactional
    public boolean insert(Tag tag) {
        return tagDao.insert(tag);
    }

    @Transactional
    public boolean insertOrUpdate(Tag tag) {
        if (tag.getId() == null || tag.getId() <= 0) {
            return tagDao.insert(tag);
        } else {
            return tagDao.update(tag);
        }
    }

    @Transactional
    public boolean update(Tag tag) {
        return tagDao.update(tag);
    }

    @Transactional
    public boolean deleteById(long id) {
        return tagDao.deleteById(id);
    }

    @Autowired
    public void setTagDao(TagDao tagDao) {
        this.tagDao = tagDao;
    }

}