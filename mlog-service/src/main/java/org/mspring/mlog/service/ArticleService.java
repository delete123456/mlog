package org.mspring.mlog.service;

import org.mspring.mlog.dao.ArticleDao;
import org.mspring.mlog.entity.Article;
import org.mspring.mlog.entity.ArticleTag;
import org.mspring.mlog.entity.Tag;
import org.mspring.mlog.entity.User;
import org.mspring.mlog.entity.em.ArticleStatus;
import org.mspring.nbee.common.utils.CollectionUtils;
import org.mspring.nbee.orm.dao.sql.builder.part.condition.SqlCondition;
import org.mspring.nbee.orm.dao.sql.pager.PageResult;
import org.mspring.nbee.orm.dao.sql.pager.Pager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author Gao Youbo
 * @since 2014-12-04 17:59:00
 */
@Service
public class ArticleService {

    private static final Logger log = LoggerFactory.getLogger(ArticleService.class);

    private ArticleDao articleDao;
    private TagService tagService;
    private ArticleTagService articleTagService;

    public Article findById(long id) {
        return articleDao.findById(id);
    }

    public PageResult<Article> list(List<SqlCondition> conditions, Pager pager) {
        return articleDao.listPage(conditions, pager);
    }

    /**
     * 创建文章
     *
     * @param article
     * @param tags
     * @param currentUser
     * @return
     */
    @Transactional
    public boolean createArticle(Article article, List<String> tags, User currentUser) {
        boolean flag = articleDao.insert(article);
        if (flag) {
            if (CollectionUtils.isNotEmpty(tags)) {
                for (String tagName : tags) {
                    Tag tag = tagService.findByName(tagName);
                    if (tag == null) {
                        tag = new Tag();
                        tag.setName(tagName);
                        tag.setCreateTime(new Date());
                        tag.setCreateUser(currentUser.getId());
                        tag.setCreateUserName(currentUser.getNickname());
                        tag.setUpdateTime(new Date());
                        tag.setUpdateUser(currentUser.getId());
                        tag.setUpdateUserName(currentUser.getNickname());
                        tagService.insertOrUpdate(tag);
                    }
                    //关联文章标签
                    ArticleTag articleTag = new ArticleTag();
                    articleTag.setArticleId(article.getId());
                    articleTag.setTagId(tag.getId());
                    articleTagService.insert(articleTag);
                }
            }
        }
        return flag;
    }

    /**
     * 更新文章
     *
     * @param article
     * @return
     */
    @Transactional
    public boolean updateArticle(Article article, List<String> tags, User currentUser) {
        boolean flag = articleDao.update(article);
        if (flag) {
            articleTagService.deleteByArticle(article.getId()); //先删除文章对应标签管理
            if (CollectionUtils.isNotEmpty(tags)) {
                for (String tagName : tags) {
                    Tag tag = tagService.findByName(tagName);
                    if (tag == null) {
                        tag = new Tag();
                        tag.setName(tagName);
                        tag.setCreateTime(new Date());
                        tag.setCreateUser(currentUser.getId());
                        tag.setCreateUserName(currentUser.getNickname());
                        tag.setUpdateTime(new Date());
                        tag.setUpdateUser(currentUser.getId());
                        tag.setUpdateUserName(currentUser.getNickname());
                        tagService.insertOrUpdate(tag);
                    }
                    //关联文章标签
                    ArticleTag articleTag = new ArticleTag();
                    articleTag.setArticleId(article.getId());
                    articleTag.setTagId(tag.getId());
                    articleTagService.insert(articleTag);
                }
            }
        }
        return flag;
    }

    @Transactional
    public boolean deleteById(long id) {
        Article article = articleDao.findById(id);
        if (article == null) {
            throw new NullPointerException("文章不存在");
        }
        article.setStatus(ArticleStatus.DELEtED);
        return articleDao.update(article);
    }

    @Autowired
    public void setArticleDao(ArticleDao articleDao) {
        this.articleDao = articleDao;
    }

    @Autowired
    public void setTagService(TagService tagService) {
        this.tagService = tagService;
    }

    @Autowired
    public void setArticleTagService(ArticleTagService articleTagService) {
        this.articleTagService = articleTagService;
    }
}