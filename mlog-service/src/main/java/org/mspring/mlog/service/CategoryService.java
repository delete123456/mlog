package org.mspring.mlog.service;

import org.mspring.mlog.dao.CategoryDao;
import org.mspring.mlog.entity.Category;
import org.mspring.nbee.orm.dao.sql.builder.part.condition.SqlCondition;
import org.mspring.nbee.orm.dao.sql.pager.PageResult;
import org.mspring.nbee.orm.dao.sql.pager.Pager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Gao Youbo
 * @since 2014-12-05 11:49:58
 */
@Service
public class CategoryService {

    private static final Logger log = LoggerFactory.getLogger(CategoryService.class);

    private CategoryDao categoryDao;

    public Category findById(long id) {
        return categoryDao.findById(id);
    }

    public PageResult<Category> list(List<SqlCondition> conditions, Pager pager) {
        return categoryDao.listPage(conditions, pager);
    }

    public List<Category> listAll() {
        return categoryDao.listAll();
    }

    @Transactional
    public boolean insert(Category category) {
        return categoryDao.insert(category);
    }

    @Transactional
    public boolean update(Category category) {
        return categoryDao.update(category);
    }

    @Transactional
    public boolean deleteById(long id) {
        return categoryDao.deleteById(id);
    }

    @Autowired
    public void setCategoryDao(CategoryDao categoryDao) {
        this.categoryDao = categoryDao;
    }

}