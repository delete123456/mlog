package org.mspring.mlog.service;

import org.mspring.mlog.dao.ArticleTagDao;
import org.mspring.mlog.entity.ArticleTag;
import org.mspring.nbee.orm.dao.sql.builder.part.condition.SqlCondition;
import org.mspring.nbee.orm.dao.sql.pager.PageResult;
import org.mspring.nbee.orm.dao.sql.pager.Pager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Gao Youbo
 * @since 2015-01-03 13:42:09
 */
@Service
public class ArticleTagService {

    private static final Logger log = LoggerFactory.getLogger(ArticleTagService.class);

    private ArticleTagDao articleTagDao;

    public ArticleTag findById(long id) {
        return articleTagDao.findById(id);
    }

    public PageResult<ArticleTag> list(List<SqlCondition> conditions, Pager pager) {
        return articleTagDao.listPage(conditions, pager);
    }

    /**
     * 删除文章对应的标签管理
     *
     * @param articleId
     * @return
     */
    @Transactional
    public boolean deleteByArticle(long articleId) {
        return articleTagDao.deleteByArticle(articleId);
    }

    @Transactional
    public boolean insert(ArticleTag articleTag) {
        return articleTagDao.insert(articleTag);
    }

    @Transactional
    public boolean update(ArticleTag articleTag) {
        return articleTagDao.update(articleTag);
    }

    @Transactional
    public boolean deleteById(long id) {
        return articleTagDao.deleteById(id);
    }

    @Autowired
    public void setArticleTagDao(ArticleTagDao articleTagDao) {
        this.articleTagDao = articleTagDao;
    }

}