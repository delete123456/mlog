package org.mspring.mlog;

import com.qiniu.api.auth.AuthException;
import com.qiniu.api.auth.digest.Mac;
import com.qiniu.api.io.IoApi;
import com.qiniu.api.io.PutExtra;
import com.qiniu.api.io.PutRet;
import com.qiniu.api.rs.PutPolicy;
import com.qiniu.api.rs.URLUtils;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * 七牛服务的封装
 *
 * @author Gao Youbo
 * @since 2014-12-30 15:37
 */
public class QiniuService {

    private static final Logger LOG = LoggerFactory.getLogger(QiniuService.class);

    private String accessKey;
    private String secretKey;
    private String bucket;
    private String domain;

    public UploadResult upload(InputStream inputStream) {
        try {
            byte[] bytes = IOUtils.toByteArray(inputStream);
            return upload(bytes);
        } catch (Exception e) {
            LOG.error("上传文件失败", e);
        }
        return null;
    }

    public UploadResult upload(byte[] data) {
        String key = DigestUtils.md5Hex(data);
        return upload(key, new ByteArrayInputStream(data));
    }

    private UploadResult upload(String key, InputStream inputStream) {
        Mac mac = new Mac(accessKey, secretKey);
        PutPolicy putPolicy = new PutPolicy(bucket);
        String uptoken = null;
        try {
            uptoken = putPolicy.token(mac);
        } catch (AuthException | JSONException e) {
            LOG.error(null, e);
        }
        PutExtra extra = new PutExtra();
        PutRet ret = IoApi.Put(uptoken, key, inputStream, extra);
        return UploadResult.from(domain, ret);
    }


    public static class UploadResult {
        private boolean success;
        private String key;
        private String hash;
        private String url;

        public static UploadResult from(String domain, PutRet putRet) {
            if (StringUtils.isBlank(domain)) {
                throw new IllegalArgumentException("domain不能为空");
            }
            UploadResult ret = new UploadResult();
            ret.setSuccess(putRet.ok());
            if (ret.isSuccess()) {
                try {
                    ret.setKey(putRet.getKey());
                    ret.setHash(putRet.getHash());
                    ret.setUrl(URLUtils.makeBaseUrl(domain, ret.getKey()));
                } catch (Exception e) {
                    LOG.error(null, e);
                }
            }
            return ret;
        }

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getHash() {
            return hash;
        }

        public void setHash(String hash) {
            this.hash = hash;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }
}
