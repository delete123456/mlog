package org.mspring.mlog.action.admin;

import org.mspring.mlog.TabItem;
import org.mspring.mlog.entity.Category;
import org.mspring.mlog.entity.User;
import org.mspring.mlog.service.CategoryService;
import org.mspring.nbee.common.utils.RequestUtils;
import org.mspring.nbee.orm.dao.sql.builder.part.condition.Operator;
import org.mspring.nbee.orm.dao.sql.builder.part.condition.SqlCondition;
import org.mspring.nbee.orm.dao.sql.pager.OrderType;
import org.mspring.nbee.orm.dao.sql.pager.PageResult;
import org.mspring.nbee.orm.dao.sql.pager.Pager;
import org.mspring.nbee.web.Action;
import org.mspring.nbee.web.exception.OperateException;
import org.mspring.nbee.web.support.QueryParam;
import org.mspring.nbee.web.support.WebContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.List;

/**
 * @author Gao Youbo
 * @since 2014-12-05 11:51
 */
@Action
@RequestMapping("/admin/category")
@TabItem(item = "100110")
public class CategoryAction extends BaseAdminAction {
    private CategoryService categoryService;

    @RequestMapping("/list")
    @TabItem(item = "100110005")
    public String list(WebContext context, Model model) {
        Pager pager = context.getPager();
        pager.addOrder("id", OrderType.DESC);
        List<SqlCondition> conditions = context.buildSqlConditions(
                model,
                new QueryParam("name", Operator.LIKE)
        );
        PageResult<Category> result = categoryService.list(conditions, pager);
        model.addAttribute("result", result);
        return "/admin/category/list";
    }

    @RequestMapping("/create")
    @TabItem(item = "100110010")
    public String create(WebContext context, Model model) {
        return "/admin/category/create";
    }

    @RequestMapping("/create/do")
    public String doCreate(WebContext context, Model model) {
        User currentUser = securityService.getCurrentUser(context.getRequest());
        Category category = context.fromRequest(Category.class);
        category.setCreateTime(new Date());
        category.setCreateUser(currentUser.getId());
        category.setCreateUserName(currentUser.getNickname());
        category.setUpdateTime(new Date());
        category.setUpdateUser(currentUser.getId());
        category.setUpdateUserName(currentUser.getNickname());
        categoryService.insert(category);
        return "redirect:/admin/category/list";
    }

    @RequestMapping("/edit")
    @TabItem(item = "100110015")
    public String edit(WebContext context, Model model) {
        Long id = RequestUtils.getParamAsLong(context.getRequest(), "id");
        if (id == null) {
            throw new OperateException("操作异常：请选择要编辑的分类");
        }
        Category category = categoryService.findById(id);
        model.addAttribute("category", category);
        return "/admin/category/edit";
    }

    @RequestMapping("/edit/do")
    public String doEdit(WebContext context, Model model) {
        User currentUser = securityService.getCurrentUser(context.getRequest());
        long id = RequestUtils.getRequiredLong(context.getRequest(), "id");
        Category category = categoryService.findById(id);
        category = context.fromRequest(category);
        category.setUpdateTime(new Date());
        category.setUpdateUser(currentUser.getId());
        category.setUpdateUserName(currentUser.getNickname());
        categoryService.update(category);
        return "redirect:/admin/category/list";
    }

    @RequestMapping("/delete/do")
    public void delete(WebContext context, Model model) {
        long id = RequestUtils.getRequiredLong(context.getRequest(), "id");
        boolean flag = categoryService.deleteById(id);
        context.renderJsonResult(flag);
    }

    @RequestMapping("/list/all")
    public void listAll(WebContext context, Model model) {
        List<Category> categoryList = categoryService.listAll();
        context.renderJsonResult(categoryList);
    }

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }
}