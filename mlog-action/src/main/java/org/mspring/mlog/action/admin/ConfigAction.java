package org.mspring.mlog.action.admin;

import org.apache.commons.lang3.StringUtils;
import org.mspring.mlog.entity.Config;
import org.mspring.mlog.entity.em.ConfigGroup;
import org.mspring.mlog.service.ConfigService;
import org.mspring.nbee.common.utils.CollectionUtils;
import org.mspring.nbee.common.utils.RequestUtils;
import org.mspring.nbee.web.Action;
import org.mspring.nbee.web.support.WebContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Gao Youbo
 * @since 2015-01-03 18:13
 */
@Action
@RequestMapping("/admin/config")
public class ConfigAction {
    private ConfigService configService;

    @RequestMapping("/base")
    public String base(WebContext context, Model model) {
        return "/admin/config/base";
    }

    @RequestMapping("/base/do")
    public void doBase(WebContext context, Model model) {
        List<Config> configs = new ArrayList<>();
        Map<String, Object> map = RequestUtils.getRequestParams(context.getRequest());
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            if (StringUtils.startsWith(entry.getKey(), "config.")) {
                String name = StringUtils.substringAfter(entry.getKey(), "config.");
                String value = null;
                if (entry.getValue() != null) {
                    if (entry.getValue() instanceof List) {
                        List<String> list = (List<String>) entry.getValue();
                        String[] array = CollectionUtils.asArray(list);
                        value = StringUtils.join(array, ",");
                    } else {
                        value = entry.getValue().toString();
                    }
                }
                Config config = new Config();
                config.setGroupName(ConfigGroup.BASE);
                config.setName(name);
                config.setValue(value);
                configs.add(config);
            }
        }
        configService.insertOrUpdateConfigs(configs);
        context.renderJsonResult(map);
    }

    @Autowired
    public void setConfigService(ConfigService configService) {
        this.configService = configService;
    }
}
