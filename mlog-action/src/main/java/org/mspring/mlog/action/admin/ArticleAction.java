package org.mspring.mlog.action.admin;

import org.mspring.mlog.QiniuService;
import org.mspring.mlog.TabItem;
import org.mspring.mlog.entity.Article;
import org.mspring.mlog.entity.Category;
import org.mspring.mlog.entity.Tag;
import org.mspring.mlog.entity.User;
import org.mspring.mlog.entity.em.ArticleStatus;
import org.mspring.mlog.service.ArticleService;
import org.mspring.mlog.service.CategoryService;
import org.mspring.mlog.service.TagService;
import org.mspring.nbee.common.utils.RequestUtils;
import org.mspring.nbee.orm.dao.sql.builder.part.condition.Operator;
import org.mspring.nbee.orm.dao.sql.builder.part.condition.SqlCondition;
import org.mspring.nbee.orm.dao.sql.builder.part.condition.impl.SimpleCondition;
import org.mspring.nbee.orm.dao.sql.pager.OrderType;
import org.mspring.nbee.orm.dao.sql.pager.PageResult;
import org.mspring.nbee.orm.dao.sql.pager.Pager;
import org.mspring.nbee.web.Action;
import org.mspring.nbee.web.exception.OperateException;
import org.mspring.nbee.web.support.QueryParam;
import org.mspring.nbee.web.support.WebContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * @author Gao Youbo
 * @since 2014-12-04 17:59
 */
@Action
@RequestMapping("/admin/article")
@TabItem(item = "100105")
public class ArticleAction extends BaseAdminAction {

    private ArticleService articleService;
    private CategoryService categoryService;
    private TagService tagService;
    private QiniuService qiniuService;

    @RequestMapping("/list")
    @TabItem(item = "100105005")
    public String list(WebContext context, Model model) {
        Pager pager = context.getPager();
        pager.addOrder("id", OrderType.DESC);
        List<SqlCondition> conditions = context.buildSqlConditions(model,
                new QueryParam("title", Operator.LIKE),
                new QueryParam("category", Operator.EQ)
        );
        conditions.add(new SimpleCondition("status", Operator.EQ, ArticleStatus.NORMAL));
        PageResult<Article> result = articleService.list(conditions, pager);
        List<Category> categoryList = categoryService.listAll();
        model.addAttribute("result", result);
        model.addAttribute("categoryList", categoryList);
        return "/admin/article/list";
    }

    @RequestMapping("/create")
    @TabItem(item = "100105010")
    public String create(WebContext context, Model model) {
        List<Category> categoryList = categoryService.listAll();
        model.addAttribute("categoryList", categoryList);
        return "/admin/article/create";
    }

    @RequestMapping("/create/do")
    public String doCreate(WebContext context, Model model) {
        User currentUser = securityService.getCurrentUser(context.getRequest());
        List<String> tags = RequestUtils.getParamAsStringList(context.getRequest(), "tags");
        Article article = context.fromRequest(Article.class);
        article.setCreateTime(new Date());
        article.setCreateUser(currentUser.getId());
        article.setCreateUserName(currentUser.getNickname());
        article.setUpdateTime(new Date());
        article.setUpdateUser(currentUser.getId());
        article.setUpdateUserName(currentUser.getNickname());
        articleService.createArticle(article, tags, currentUser);
        return "redirect:/admin/article/list";
    }


    @RequestMapping("/edit")
    @TabItem(item = "100105015")
    public String edit(WebContext context, Model model) {
        Long id = RequestUtils.getParamAsLong(context.getRequest(), "id");
        if (id == null) {
            throw new OperateException("操作异常：请选择要编辑的文章");
        }
        Article article = articleService.findById(id);
        List<Category> categoryList = categoryService.listAll();
        List<Tag> tagList = tagService.listByArticle(id);
        model.addAttribute("article", article);
        model.addAttribute("categoryList", categoryList);
        model.addAttribute("tagList", tagList);
        return "/admin/article/edit";
    }


    @RequestMapping("/edit/do")
    public String doEdit(WebContext context, Model model) {
        User currentUser = securityService.getCurrentUser(context.getRequest());
        long id = RequestUtils.getRequiredLong(context.getRequest(), "id");
        List<String> tags = RequestUtils.getParamAsStringList(context.getRequest(), "tags");
        Article article = articleService.findById(id);
        article = context.fromRequest(article);
        article.setUpdateTime(new Date());
        article.setUpdateUser(currentUser.getId());
        article.setUpdateUserName(currentUser.getNickname());
        articleService.updateArticle(article, tags, currentUser);
        return "redirect:/admin/article/list";
    }

    @RequestMapping("/edit/upload")
    public void upload(@RequestParam MultipartFile image, WebContext context, Model model) throws IOException, ServletException {
        QiniuService.UploadResult result = qiniuService.upload(image.getBytes());
        context.renderJsonResult(result);
    }

    @RequestMapping("/delete/do")
    public void delete(WebContext context, Model model) {
        long id = RequestUtils.getRequiredLong(context.getRequest(), "id");
        boolean flag = articleService.deleteById(id);
        context.renderJsonResult(flag);
    }

    @Autowired
    public void setArticleService(ArticleService articleService) {
        this.articleService = articleService;
    }

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @Autowired
    public void setTagService(TagService tagService) {
        this.tagService = tagService;
    }

    @Autowired
    public void setQiniuService(QiniuService qiniuService) {
        this.qiniuService = qiniuService;
    }
}
