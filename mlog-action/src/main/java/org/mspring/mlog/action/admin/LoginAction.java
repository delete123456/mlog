package org.mspring.mlog.action.admin;

import org.mspring.mlog.security.SecurityService;
import org.mspring.nbee.web.Action;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Gao Youbo
 * @since 2014-10-11 14:09:00
 */
@Action
@RequestMapping("/admin")
public class LoginAction extends BaseAdminAction {
    private SecurityService securityService;

    @RequestMapping("/login")
    public String login() {

        return "/admin/login";
    }

    @RequestMapping(value = "/login/do", method = RequestMethod.POST)
    public String doLogin(@RequestParam String username, String password, HttpServletRequest request) {
        securityService.login(username, password, request);
        return "redirect:/admin/index";
    }

    @Autowired
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }

}
