package org.mspring.mlog.action.admin;

import org.mspring.nbee.web.Action;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Gao Youbo
 * @since 2014年10月13日
 */
@Action
@RequestMapping("/admin")
public class IndexAction extends BaseAdminAction {

    @RequestMapping({"/index", "/", ""})
    public String index() {
        return "/admin/index";
    }

}
