package org.mspring.mlog.action.admin;

import org.mspring.mlog.TabItem;
import org.mspring.nbee.web.Action;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Gao Youbo
 * @since 2014年10月22日
 */
@Action
@RequestMapping("/admin")
@TabItem(item = "910")
public class ContactAction extends BaseAdminAction {
    @RequestMapping("/contact")
    @TabItem(item = "910005")
    public String contact(HttpServletRequest request, HttpServletResponse response, Model model) {
        return "/admin/contact";
    }
}
