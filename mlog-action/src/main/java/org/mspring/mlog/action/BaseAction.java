package org.mspring.mlog.action;

import org.mspring.mlog.security.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Gao Youbo
 * @since 2014-12-05 13:31
 */
public class BaseAction {
    protected SecurityService securityService;


    @Autowired
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }
}
