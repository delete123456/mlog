package org.mspring.mlog.action.admin;

import org.mspring.mlog.entity.Node;
import org.mspring.mlog.entity.User;
import org.mspring.mlog.security.SecurityService;
import org.mspring.mlog.security.service.NodeService;
import org.mspring.nbee.common.utils.RequestUtils;
import org.mspring.nbee.web.Action;
import org.mspring.nbee.web.support.WebContext;
import org.mspring.nbee.web.view.json.JsonResultRender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @author Gao Youbo
 * @since 2014年10月24日
 */
@Action
@RequestMapping("/admin")
public class MenuAction extends BaseAdminAction {
    private SecurityService securityService;
    private NodeService nodeService;

    @RequestMapping("/tabs")
    public void tabs(WebContext context) {
        String id = RequestUtils.getRequiredParam(context.getRequest(), "id");
        User user = securityService.getCurrentUser(context.getRequest());
        List<Node> tabs = nodeService.listTabByUser(id, user.getId());
        JsonResultRender.render(context.getResponse(), tabs);
    }

    @Autowired
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }

    @Autowired
    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

}
