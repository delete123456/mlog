package org.mspring.mlog.action.admin;

import org.apache.commons.lang3.StringUtils;
import org.mspring.mlog.TabItem;
import org.mspring.mlog.entity.Tag;
import org.mspring.mlog.entity.User;
import org.mspring.mlog.service.TagService;
import org.mspring.nbee.common.utils.RequestUtils;
import org.mspring.nbee.orm.dao.sql.builder.part.condition.Operator;
import org.mspring.nbee.orm.dao.sql.builder.part.condition.SqlCondition;
import org.mspring.nbee.orm.dao.sql.pager.OrderType;
import org.mspring.nbee.orm.dao.sql.pager.PageResult;
import org.mspring.nbee.orm.dao.sql.pager.Pager;
import org.mspring.nbee.web.Action;
import org.mspring.nbee.web.exception.OperateException;
import org.mspring.nbee.web.support.QueryParam;
import org.mspring.nbee.web.support.WebContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Gao Youbo
 * @since 2015-01-02 15:54
 */
@Action
@RequestMapping("/admin/tag")
@TabItem(item = "100115")
public class TagAction extends BaseAdminAction {
    private TagService tagService;

    @RequestMapping("/list")
    @TabItem(item = "100115005")
    public String list(WebContext context, Model model) {
        Pager pager = context.getPager();
        pager.addOrder("id", OrderType.DESC);
        List<SqlCondition> conditions = context.buildSqlConditions(
                model,
                new QueryParam("name", Operator.LIKE)
        );
        PageResult<Tag> result = tagService.list(conditions, pager);
        model.addAttribute("result", result);
        return "/admin/tag/list";
    }

    @RequestMapping("/create")
    @TabItem(item = "100115010")
    public String create(WebContext context, Model model) {
        return "/admin/tag/create";
    }

    @RequestMapping("/create/do")
    public String doCreate(WebContext context, Model model) {
        User currentUser = securityService.getCurrentUser(context.getRequest());
        Tag tag = context.fromRequest(Tag.class);
        tag.setCreateTime(new Date());
        tag.setCreateUser(currentUser.getId());
        tag.setCreateUserName(currentUser.getNickname());
        tag.setUpdateTime(new Date());
        tag.setUpdateUser(currentUser.getId());
        tag.setUpdateUserName(currentUser.getNickname());
        tagService.insert(tag);
        return "redirect:/admin/tag/list";
    }

    @RequestMapping("/edit")
    @TabItem(item = "100115015")
    public String edit(WebContext context, Model model) {
        Long id = RequestUtils.getParamAsLong(context.getRequest(), "id");
        if (id == null) {
            throw new OperateException("操作异常：请选择要编辑的分类");
        }
        Tag tag = tagService.findById(id);
        model.addAttribute("tag", tag);
        return "/admin/tag/edit";
    }

    @RequestMapping("/edit/do")
    public String doEdit(WebContext context, Model model) {
        User currentUser = securityService.getCurrentUser(context.getRequest());
        long id = RequestUtils.getRequiredLong(context.getRequest(), "id");
        Tag tag = tagService.findById(id);
        tag = context.fromRequest(tag);
        tag.setUpdateTime(new Date());
        tag.setUpdateUser(currentUser.getId());
        tag.setUpdateUserName(currentUser.getNickname());
        tagService.update(tag);
        return "redirect:/admin/tag/list";
    }

    @RequestMapping("/delete/do")
    public void delete(WebContext context, Model model) {
        long id = RequestUtils.getRequiredLong(context.getRequest(), "id");
        boolean flag = tagService.deleteById(id);
        context.renderJsonResult(flag);
    }

    @RequestMapping("/autocomplete")
    public void autocomplete(WebContext context) {
        List<Tag> tags = new ArrayList<>();
        String name = RequestUtils.getParam(context.getRequest(), "name");
        if (StringUtils.isNotBlank(name)) {
            tags = tagService.listByName(name);
        }
        tags = tagService.listByName("s");
        context.renderJsonResult(tags);
    }

    @Autowired
    public void setTagService(TagService tagService) {
        this.tagService = tagService;
    }
}
