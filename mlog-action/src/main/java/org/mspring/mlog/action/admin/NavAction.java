package org.mspring.mlog.action.admin;

import org.mspring.mlog.entity.Node;
import org.mspring.mlog.entity.User;
import org.mspring.mlog.security.SecurityService;
import org.mspring.mlog.security.service.NodeService;
import org.mspring.nbee.web.Action;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 导航
 *
 * @author Gao Youbo
 * @since 2014年10月13日
 */
@Action
@RequestMapping("/admin/nav")
public class NavAction extends BaseAdminAction {
    private NodeService nodeService;
    private SecurityService securityService;

    @RequestMapping(value = "/leftMenu")
    public String execute(HttpServletRequest request, HttpServletResponse response, Model model) {
        User user = securityService.getCurrentUser(request);
        List<Node> items = nodeService.listTreeByUser(user.getId());
        model.addAttribute("items", items);
        return "/admin/frame/leftMenu";
    }

    @Autowired
    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    @Autowired
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }

}
