package org.mspring.mlog.action.admin;

import org.mspring.mlog.TabItem;
import org.mspring.mlog.entity.Comment;
import org.mspring.mlog.entity.User;
import org.mspring.mlog.service.CommentService;
import org.mspring.nbee.common.utils.RequestUtils;
import org.mspring.nbee.orm.dao.sql.builder.part.condition.Operator;
import org.mspring.nbee.orm.dao.sql.builder.part.condition.SqlCondition;
import org.mspring.nbee.orm.dao.sql.pager.OrderType;
import org.mspring.nbee.orm.dao.sql.pager.PageResult;
import org.mspring.nbee.orm.dao.sql.pager.Pager;
import org.mspring.nbee.web.Action;
import org.mspring.nbee.web.exception.OperateException;
import org.mspring.nbee.web.support.QueryParam;
import org.mspring.nbee.web.support.WebContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @author Gao Youbo
 * @since 2015-01-03 14:41
 */
@Action
@RequestMapping("/admin/comment")
@TabItem(item = "100120")
public class CommentAction extends BaseAdminAction {
    private CommentService commentService;

    @RequestMapping("/list")
    @TabItem(item = "100120005")
    public String list(WebContext context, Model model) {
        Pager pager = context.getPager();
        pager.addOrder("id", OrderType.DESC);
        List<SqlCondition> conditions = context.buildSqlConditions(model,
                new QueryParam("content", Operator.LIKE)
        );
        PageResult<Comment> result = commentService.list(conditions, pager);
        model.addAttribute("result", result);
        return "/admin/comment/list";
    }


    @RequestMapping("/edit")
    @TabItem(item = "100120015")
    public String edit(WebContext context, Model model) {
        Long id = RequestUtils.getParamAsLong(context.getRequest(), "id");
        if (id == null) {
            throw new OperateException("操作异常：请选择要编辑的评论");
        }
        Comment comment = commentService.findById(id);
        model.addAttribute("comment", comment);
        return "/admin/comment/edit";
    }


    @RequestMapping("/edit/do")
    public String doEdit(WebContext context, Model model) {
        long id = RequestUtils.getRequiredLong(context.getRequest(), "id");
        Comment comment = commentService.findById(id);
        comment = context.fromRequest(comment);
        commentService.update(comment);
        return "redirect:/admin/comment/list";
    }

    @RequestMapping("/delete/do")
    public void delete(WebContext context, Model model) {
        long id = RequestUtils.getRequiredLong(context.getRequest(), "id");
        boolean flag = commentService.deleteById(id);
        context.renderJsonResult(flag);
    }

    @Autowired
    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }
}
