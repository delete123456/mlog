package org.mspring.mlog.action.admin;

import org.mspring.mlog.entity.User;
import org.mspring.nbee.web.Action;
import org.mspring.nbee.web.view.json.JsonResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Gao Youbo
 * @since 2014-10-10 19:03:19
 */
@Action
public class TestAction extends BaseAdminAction {

    @RequestMapping("/json")
    @ResponseBody
    public Object json() {
        User user = new User();
        user.setUsername("高尤波");
        return user;
    }

    @ResponseBody
    @RequestMapping("/fuck")
    public Object fuck() {
        User user = new User();
        user.setUsername("高尤波");
        Map<String, Object> map = new HashMap<>();
        map.put("fuck", "fuck");
        map.put("user", user);
        return map;
    }

    @ResponseBody
    @RequestMapping("/shit")
    public Object shit() {
        JsonResult result = new JsonResult();
        result.setMessage("fuck");
        result.setData("haha");
        return result;
    }
}
