package org.mspring.mlog.action.admin;

import org.mspring.mlog.Application;
import org.mspring.mlog.TabItem;
import org.mspring.nbee.web.Action;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Gao Youbo
 * @since 2014年10月22日
 */
@Action
@RequestMapping("/admin")
public class AboutAction extends BaseAdminAction {

    /**
     * 关于
     *
     * @param request
     * @param response
     * @param model
     * @return
     */
    @RequestMapping("/about")
    @TabItem(item = "905005")
    public String about(HttpServletRequest request, HttpServletResponse response, Model model) {
        Application app = Application.getInstance();
        model.addAttribute("app", app);

        float fFreeMemory = Runtime.getRuntime().freeMemory();
        float fTotalMemory = Runtime.getRuntime().totalMemory();
        float fUsedMemory = fTotalMemory - fFreeMemory;
        float fPercent = fFreeMemory / fTotalMemory * 100;
        String serverName = request.getServerName();
        String remoteAddr = request.getRemoteAddr();
        String os = System.getProperty("os.name");
        String javaVersion = System.getProperty("java.version");

        model.addAttribute("fFreeMemory", fFreeMemory);
        model.addAttribute("fTotalMemory", fTotalMemory);
        model.addAttribute("fUsedMemory", fUsedMemory);
        model.addAttribute("fPercent", fPercent);
        model.addAttribute("serverName", serverName);
        model.addAttribute("remoteAddr", remoteAddr);
        model.addAttribute("os", os);
        model.addAttribute("javaVersion", javaVersion);

        return "/admin/about";
    }
}
