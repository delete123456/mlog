package org.mspring.mlog.action;

import org.mspring.mlog.entity.Node;
import org.mspring.mlog.entity.RoleNode;
import org.mspring.mlog.entity.em.NodeType;
import org.mspring.mlog.imports.ImportService;
import org.mspring.mlog.security.service.NodeService;
import org.mspring.mlog.security.service.RoleNodeService;
import org.mspring.nbee.web.Action;
import org.mspring.nbee.web.support.WebContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Gao Youbo
 * @since 2015-01-02 14:23
 */
@Action
@RequestMapping("/admin/tools")
public class ToolsAction {
    private NodeService nodeService;
    private RoleNodeService roleNodeService;
    private ImportService importService;


    @RequestMapping("/init-node")
    public void initNode(WebContext context) {
        roleNodeService.deleteAll();
        nodeService.deleteAll();

        List<Node> nodes = new ArrayList<>();
        nodes.add(createNode("内容", "", "100", "0", NodeType.TREE_FOLDER, true, true));
        nodes.add(createNode("文章管理", "", "100105", "100", NodeType.TREE_ITEM, true, true));
        nodes.add(createNode("列表", "/admin/article/list", "100105005", "100105", NodeType.TAB, true, true));
        nodes.add(createNode("添加", "/admin/article/create", "100105010", "100105", NodeType.TAB, false, true));
        nodes.add(createNode("修改", "/admin/article/edit", "100105015", "100105", NodeType.TAB, false, false));

        nodes.add(createNode("分类管理", "", "100110", "100", NodeType.TREE_ITEM, true, true));
        nodes.add(createNode("列表", "/admin/category/list", "100110005", "100110", NodeType.TAB, true, true));
        nodes.add(createNode("添加", "/admin/category/create", "100110010", "100110", NodeType.TAB, false, true));
        nodes.add(createNode("修改", "/admin/category/edit", "100110015", "100110", NodeType.TAB, false, false));

        nodes.add(createNode("标签管理", "", "100115", "100", NodeType.TREE_ITEM, true, true));
        nodes.add(createNode("列表", "/admin/tag/list", "100115005", "100115", NodeType.TAB, true, true));
        nodes.add(createNode("添加", "/admin/tag/create", "100115010", "100115", NodeType.TAB, false, true));
        nodes.add(createNode("修改", "/admin/tag/edit", "100115015", "100115", NodeType.TAB, false, false));

        nodes.add(createNode("评论管理", "", "100120", "100", NodeType.TREE_ITEM, true, true));
        nodes.add(createNode("列表", "/admin/comment/list", "100120005", "100120", NodeType.TAB, true, true));
        nodes.add(createNode("修改", "/admin/comment/edit", "100120015", "100120", NodeType.TAB, false, false));

        nodes.add(createNode("设置", "", "200", "0", NodeType.TREE_FOLDER, true, true));
        nodes.add(createNode("通用配置", "", "200105", "200", NodeType.TREE_ITEM, true, true));
        nodes.add(createNode("通用配置", "/admin/config/base", "200105005", "200105", NodeType.TAB, true, true));

        nodes.add(createNode("关于", "", "900", "0", NodeType.TREE_FOLDER, true, true));
        nodes.add(createNode("关于", "", "900905", "900", NodeType.TREE_ITEM, true, true));
        nodes.add(createNode("关于", "/admin/about", "900905005", "900905", NodeType.TAB, true, true));
        nodes.add(createNode("联系我们", "", "900910", "900", NodeType.TREE_ITEM, true, true));
        nodes.add(createNode("联系我们", "/admin/contact", "900910005", "900910", NodeType.TAB, true, true));

        for (Node node : nodes) {
            nodeService.insert(node);

            RoleNode rn = new RoleNode();
            rn.setRoleId(1L);
            rn.setNodeId(node.getId());
            roleNodeService.insert(rn);
        }
        context.renderJsonResult(true);
    }

    @RequestMapping("/import")
    public void importData(WebContext context) {
        importService.importCategory();
        importService.importArticle();
        context.renderJsonResult(true);
    }

    private Node createNode(String name, String url, String id, String parent, String type, boolean open, boolean clickable) {
        Node node = new Node();
        node.setName(name);
        node.setUrl(url);
        node.setId(id);
        node.setParent(parent);
        node.setType(type);
        node.setOpen(open);
        node.setClickable(clickable);
        return node;
    }

    @Autowired
    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    @Autowired
    public void setRoleNodeService(RoleNodeService roleNodeService) {
        this.roleNodeService = roleNodeService;
    }

    @Autowired
    public void setImportService(ImportService importService) {
        this.importService = importService;
    }
}
