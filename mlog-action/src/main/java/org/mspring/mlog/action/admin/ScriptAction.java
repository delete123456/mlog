package org.mspring.mlog.action.admin;

import freemarker.template.Template;
import org.mspring.nbee.common.utils.FreemarkerUtils;
import org.mspring.nbee.web.Action;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Gao Youbo
 * @since 2014-12-12 15:56
 */
@Action
@RequestMapping("/admin/script")
public class ScriptAction extends BaseAdminAction {

    private FreeMarkerConfigurer freeMarkerConfigurer;

    @RequestMapping("/variable")
    public void variable(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        Template template = freeMarkerConfigurer.getConfiguration().getTemplate("/WEB-INF/template/variable.ftl");
        model.addAttribute("base", request.getContextPath());
        String content = FreemarkerUtils.render(template, model);
        response.setHeader("contextType", "text/javascript");
        response.getWriter().write(content);
    }

    @Autowired
    public void setFreeMarkerConfigurer(FreeMarkerConfigurer freeMarkerConfigurer) {
        this.freeMarkerConfigurer = freeMarkerConfigurer;
    }
}
