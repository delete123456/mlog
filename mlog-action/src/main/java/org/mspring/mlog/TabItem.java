package org.mspring.mlog;

import java.lang.annotation.*;

/**
 * @author Gao Youbo
 * @since 2014-12-22 18:41
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TabItem {

    String item();
}
