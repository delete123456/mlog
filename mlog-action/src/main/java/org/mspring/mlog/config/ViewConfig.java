package org.mspring.mlog.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;

/**
 * @author Gao Youbo
 * @since 2015-01-05 12:46
 */
//@EnableWebMvc
@Configuration
@ComponentScan({"org.mspring.*"})
@Import({SecurityConfig.class})
@ImportResource({"classpath:applicationContext.xml", "classpath:applicationContext-dispatcher.xml"})
public class ViewConfig {
//    @Bean
//    public ExceptionHandler exceptionHandler() {
//        return new ExceptionHandler();
//    }
//
//    @Bean
//    public RequestMappingHandlerMapping requestMappingHandlerMapping() {
//        RequestMappingHandlerMapping mapping = new RequestMappingHandlerMapping();
//        Object[] interceptors = new Object[]{
//                new SettingInterceptor(),
//                new TabItemInterceptor()
//        };
//        mapping.setInterceptors(interceptors);
//        return mapping;
//    }
//
//    @Bean
//    public RequestMappingHandlerAdapter requestMappingHandlerAdapter() {
//
//    }
}
