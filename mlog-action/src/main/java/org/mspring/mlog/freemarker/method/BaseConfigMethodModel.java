package org.mspring.mlog.freemarker.method;

import org.mspring.mlog.entity.em.ConfigGroup;
import org.mspring.nbee.common.utils.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Gao Youbo
 * @since 2015-01-04 14:06
 */
@Component
public class BaseConfigMethodModel extends ConfigMethodModel {

    @Override
    protected String getGroup(List arguments) {
        return ConfigGroup.BASE;
    }

    @Override
    protected String getName(List arguments) {
        if (CollectionUtils.isEmpty(arguments)) {
            return null;
        }
        return arguments.get(0).toString();
    }
}
