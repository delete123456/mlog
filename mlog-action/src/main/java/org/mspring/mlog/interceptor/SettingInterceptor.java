package org.mspring.mlog.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 *
 * @author Gao Youbo
 * @since 2014年10月13日
 */
public class SettingInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        request.setAttribute("base", request.getContextPath());
        return super.preHandle(request, response, handler);
    }
}