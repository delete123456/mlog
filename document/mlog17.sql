/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50527
Source Host           : 127.0.0.1:3306
Source Database       : mlog17

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2015-01-03 18:15:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_article
-- ----------------------------
DROP TABLE IF EXISTS `t_article`;
CREATE TABLE `t_article` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category` bigint(20) NOT NULL,
  `title` varchar(1024) NOT NULL,
  `content` longtext NOT NULL,
  `image_list` text,
  `status` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `create_user` bigint(20) NOT NULL,
  `create_user_name` varchar(32) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_user` bigint(20) NOT NULL,
  `update_user_name` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Reference_9` (`category`),
  CONSTRAINT `FK_Reference_9` FOREIGN KEY (`category`) REFERENCES `t_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_article
-- ----------------------------
INSERT INTO `t_article` VALUES ('1', '1', 'sdf', 'sss', '', '1', '2015-01-03 14:11:03', '1', '雾非雾的情思', '2015-01-03 14:11:03', '1', '雾非雾的情思');
INSERT INTO `t_article` VALUES ('2', '1', '111', '111', '', '1', '2015-01-03 14:12:00', '1', '雾非雾的情思', '2015-01-03 14:12:00', '1', '雾非雾的情思');
INSERT INTO `t_article` VALUES ('3', '1', 'java', 'java', '', '0', '2015-01-03 14:13:44', '1', '雾非雾的情思', '2015-01-03 14:13:44', '1', '雾非雾的情思');
INSERT INTO `t_article` VALUES ('4', '2', 'javascript', 'js', '', '0', '2015-01-03 14:21:08', '1', '雾非雾的情思', '2015-01-03 14:29:36', '1', '雾非雾的情思');

-- ----------------------------
-- Table structure for t_article_tag
-- ----------------------------
DROP TABLE IF EXISTS `t_article_tag`;
CREATE TABLE `t_article_tag` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `article_id` bigint(20) NOT NULL,
  `tag_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique` (`article_id`,`tag_id`),
  KEY `FK_Reference_8` (`tag_id`),
  CONSTRAINT `FK_Reference_7` FOREIGN KEY (`article_id`) REFERENCES `t_article` (`id`),
  CONSTRAINT `FK_Reference_8` FOREIGN KEY (`tag_id`) REFERENCES `t_tag` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_article_tag
-- ----------------------------
INSERT INTO `t_article_tag` VALUES ('1', '3', '3');
INSERT INTO `t_article_tag` VALUES ('15', '4', '6');
INSERT INTO `t_article_tag` VALUES ('16', '4', '7');
INSERT INTO `t_article_tag` VALUES ('17', '4', '8');

-- ----------------------------
-- Table structure for t_category
-- ----------------------------
DROP TABLE IF EXISTS `t_category`;
CREATE TABLE `t_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(12) NOT NULL,
  `deleted` bit(1) NOT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `create_user` bigint(20) NOT NULL,
  `create_user_name` varchar(32) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_user` bigint(20) NOT NULL,
  `update_user_name` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_category
-- ----------------------------
INSERT INTO `t_category` VALUES ('1', 'Java', '\0', '', '2014-12-29 16:57:06', '1', '雾非雾的情思', '2014-12-29 16:57:06', '1', '雾非雾的情思');
INSERT INTO `t_category` VALUES ('2', 'JavaScript', '\0', '', '2015-01-01 16:50:56', '1', '雾非雾的情思', '2015-01-01 16:50:56', '1', '雾非雾的情思');
INSERT INTO `t_category` VALUES ('7', 'fuckthis', '\0', '', '2015-01-02 12:43:14', '1', '雾非雾的情思', '2015-01-02 12:44:48', '1', '雾非雾的情思');

-- ----------------------------
-- Table structure for t_comment
-- ----------------------------
DROP TABLE IF EXISTS `t_comment`;
CREATE TABLE `t_comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `place` varchar(32) NOT NULL,
  `topic_id` bigint(20) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `content` text NOT NULL,
  `author_name` varchar(32) NOT NULL,
  `author_avatar` varchar(1024) DEFAULT NULL,
  `author_email` varchar(1024) DEFAULT NULL,
  `author_url` varchar(512) DEFAULT NULL,
  `ip` varchar(32) DEFAULT NULL,
  `renferer` varchar(1024) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_place` (`place`),
  KEY `idx_topic` (`topic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_comment
-- ----------------------------

-- ----------------------------
-- Table structure for t_config
-- ----------------------------
DROP TABLE IF EXISTS `t_config`;
CREATE TABLE `t_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `config_group` varchar(32) NOT NULL,
  `name` varchar(64) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_name` (`name`),
  KEY `idx_group_name` (`config_group`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_node
-- ----------------------------
DROP TABLE IF EXISTS `t_node`;
CREATE TABLE `t_node` (
  `id` varchar(32) NOT NULL,
  `parent` varchar(32) DEFAULT NULL,
  `type` varchar(16) NOT NULL,
  `url` varchar(512) DEFAULT NULL,
  `deleted` bit(1) NOT NULL,
  `icon` varchar(512) DEFAULT NULL,
  `name` varchar(64) NOT NULL,
  `open` bit(1) NOT NULL,
  `clickable` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_node
-- ----------------------------
INSERT INTO `t_node` VALUES ('100', '0', 'tree_folder', '', '\0', null, '内容', '', '');
INSERT INTO `t_node` VALUES ('100105', '100', 'tree_item', '', '\0', null, '文章管理', '', '');
INSERT INTO `t_node` VALUES ('100105005', '100105', 'tab', '/admin/article/list', '\0', null, '列表', '', '');
INSERT INTO `t_node` VALUES ('100105010', '100105', 'tab', '/admin/article/create', '\0', null, '添加', '\0', '');
INSERT INTO `t_node` VALUES ('100105015', '100105', 'tab', '/admin/article/edit', '\0', null, '修改', '\0', '\0');
INSERT INTO `t_node` VALUES ('100110', '100', 'tree_item', '', '\0', null, '分类管理', '', '');
INSERT INTO `t_node` VALUES ('100110005', '100110', 'tab', '/admin/category/list', '\0', null, '列表', '', '');
INSERT INTO `t_node` VALUES ('100110010', '100110', 'tab', '/admin/category/create', '\0', null, '添加', '\0', '');
INSERT INTO `t_node` VALUES ('100110015', '100110', 'tab', '/admin/category/edit', '\0', null, '修改', '\0', '\0');
INSERT INTO `t_node` VALUES ('100115', '100', 'tree_item', '', '\0', null, '标签管理', '', '');
INSERT INTO `t_node` VALUES ('100115005', '100115', 'tab', '/admin/tag/list', '\0', null, '列表', '', '');
INSERT INTO `t_node` VALUES ('100115010', '100115', 'tab', '/admin/tag/create', '\0', null, '添加', '\0', '');
INSERT INTO `t_node` VALUES ('100115015', '100115', 'tab', '/admin/tag/edit', '\0', null, '修改', '\0', '\0');
INSERT INTO `t_node` VALUES ('100120', '100', 'tree_item', '', '\0', null, '评论管理', '', '');
INSERT INTO `t_node` VALUES ('100120005', '100120', 'tab', '/admin/comment/list', '\0', null, '列表', '', '');
INSERT INTO `t_node` VALUES ('100120015', '100120', 'tab', '/admin/comment/edit', '\0', null, '修改', '\0', '\0');
INSERT INTO `t_node` VALUES ('200', '0', 'tree_folder', '', '\0', null, '设置', '', '');
INSERT INTO `t_node` VALUES ('200105', '200', 'tree_item', '', '\0', null, '通用配置', '', '');
INSERT INTO `t_node` VALUES ('200105005', '200105', 'tab', '/admin/config/base', '\0', null, '通用配置', '', '');
INSERT INTO `t_node` VALUES ('900', '0', 'tree_folder', '', '\0', null, '关于', '', '');
INSERT INTO `t_node` VALUES ('900905', '900', 'tree_item', '', '\0', null, '关于', '', '');
INSERT INTO `t_node` VALUES ('900905005', '900905', 'tab', '/admin/about', '\0', null, '关于', '', '');
INSERT INTO `t_node` VALUES ('900910', '900', 'tree_item', '', '\0', null, '联系我们', '', '');
INSERT INTO `t_node` VALUES ('900910005', '900910', 'tab', '/admin/contact', '\0', null, '联系我们', '', '');

-- ----------------------------
-- Table structure for t_resource
-- ----------------------------
DROP TABLE IF EXISTS `t_resource`;
CREATE TABLE `t_resource` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `node_id` varchar(32) DEFAULT NULL,
  `type` varchar(32) DEFAULT NULL,
  `url` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_resource
-- ----------------------------
INSERT INTO `t_resource` VALUES ('1', '系统后台', '', null, '/admin/*');
INSERT INTO `t_resource` VALUES ('2', '用户中心', null, null, '/u/*/admin/**');

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `enabled` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES ('1', '网站创始人', '');
INSERT INTO `t_role` VALUES ('2', '系统管理员', '');
INSERT INTO `t_role` VALUES ('3', '投稿者', '');

-- ----------------------------
-- Table structure for t_role_node
-- ----------------------------
DROP TABLE IF EXISTS `t_role_node`;
CREATE TABLE `t_role_node` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NOT NULL,
  `node_id` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_role_node` (`role_id`,`node_id`),
  KEY `FK_treeitem_role_treeitem` (`node_id`),
  CONSTRAINT `FK_role_role_treeitem` FOREIGN KEY (`role_id`) REFERENCES `t_role` (`id`),
  CONSTRAINT `FK_treeitem_role_treeitem` FOREIGN KEY (`node_id`) REFERENCES `t_node` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=320 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_role_node
-- ----------------------------
INSERT INTO `t_role_node` VALUES ('296', '1', '100');
INSERT INTO `t_role_node` VALUES ('297', '1', '100105');
INSERT INTO `t_role_node` VALUES ('298', '1', '100105005');
INSERT INTO `t_role_node` VALUES ('299', '1', '100105010');
INSERT INTO `t_role_node` VALUES ('300', '1', '100105015');
INSERT INTO `t_role_node` VALUES ('301', '1', '100110');
INSERT INTO `t_role_node` VALUES ('302', '1', '100110005');
INSERT INTO `t_role_node` VALUES ('303', '1', '100110010');
INSERT INTO `t_role_node` VALUES ('304', '1', '100110015');
INSERT INTO `t_role_node` VALUES ('305', '1', '100115');
INSERT INTO `t_role_node` VALUES ('306', '1', '100115005');
INSERT INTO `t_role_node` VALUES ('307', '1', '100115010');
INSERT INTO `t_role_node` VALUES ('308', '1', '100115015');
INSERT INTO `t_role_node` VALUES ('309', '1', '100120');
INSERT INTO `t_role_node` VALUES ('310', '1', '100120005');
INSERT INTO `t_role_node` VALUES ('311', '1', '100120015');
INSERT INTO `t_role_node` VALUES ('312', '1', '200');
INSERT INTO `t_role_node` VALUES ('313', '1', '200105');
INSERT INTO `t_role_node` VALUES ('314', '1', '200105005');
INSERT INTO `t_role_node` VALUES ('315', '1', '900');
INSERT INTO `t_role_node` VALUES ('316', '1', '900905');
INSERT INTO `t_role_node` VALUES ('317', '1', '900905005');
INSERT INTO `t_role_node` VALUES ('318', '1', '900910');
INSERT INTO `t_role_node` VALUES ('319', '1', '900910005');

-- ----------------------------
-- Table structure for t_role_resource
-- ----------------------------
DROP TABLE IF EXISTS `t_role_resource`;
CREATE TABLE `t_role_resource` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NOT NULL,
  `resource_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_role_resource` (`role_id`,`resource_id`),
  KEY `FK_resource_role_resource` (`resource_id`),
  CONSTRAINT `FK_resource_role_resource` FOREIGN KEY (`resource_id`) REFERENCES `t_resource` (`id`),
  CONSTRAINT `FK_role_role_resource` FOREIGN KEY (`role_id`) REFERENCES `t_role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_role_resource
-- ----------------------------
INSERT INTO `t_role_resource` VALUES ('1', '1', '1');
INSERT INTO `t_role_resource` VALUES ('2', '1', '2');
INSERT INTO `t_role_resource` VALUES ('3', '2', '1');
INSERT INTO `t_role_resource` VALUES ('4', '3', '1');

-- ----------------------------
-- Table structure for t_tag
-- ----------------------------
DROP TABLE IF EXISTS `t_tag`;
CREATE TABLE `t_tag` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `create_user` bigint(20) NOT NULL,
  `create_user_name` varchar(32) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_user` bigint(20) NOT NULL,
  `update_user_name` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_tag
-- ----------------------------
INSERT INTO `t_tag` VALUES ('3', 'java', null, '2015-01-03 14:13:50', '1', '雾非雾的情思', '2015-01-03 14:13:50', '1', '雾非雾的情思');
INSERT INTO `t_tag` VALUES ('4', 'js', null, '2015-01-03 14:21:08', '1', '雾非雾的情思', '2015-01-03 14:21:08', '1', '雾非雾的情思');
INSERT INTO `t_tag` VALUES ('5', 'javascript', null, '2015-01-03 14:21:08', '1', '雾非雾的情思', '2015-01-03 14:21:08', '1', '雾非雾的情思');
INSERT INTO `t_tag` VALUES ('6', 'fuck', null, '2015-01-03 14:27:23', '1', '雾非雾的情思', '2015-01-03 14:27:23', '1', '雾非雾的情思');
INSERT INTO `t_tag` VALUES ('7', 'shit', null, '2015-01-03 14:28:43', '1', '雾非雾的情思', '2015-01-03 14:28:44', '1', '雾非雾的情思');
INSERT INTO `t_tag` VALUES ('8', 'name', null, '2015-01-03 14:28:47', '1', '雾非雾的情思', '2015-01-03 14:28:47', '1', '雾非雾的情思');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(32) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(64) NOT NULL,
  `email` varchar(128) NOT NULL,
  `avatar` varchar(1024) DEFAULT NULL,
  `small_avatar` varchar(1024) DEFAULT NULL,
  `deleted` bit(1) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', '雾非雾的情思', 'gaoyoubo', 'gaoyoubo', 'gaoyoubo@mspring.org', '/avatar/2013/6/avatar-1_large.jpg', '/avatar/2013/6/avatar-1_small.jpg', '\0', '2013-01-11 12:02:49', '2014-09-10 13:18:31');

-- ----------------------------
-- Table structure for t_user_role
-- ----------------------------
DROP TABLE IF EXISTS `t_user_role`;
CREATE TABLE `t_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_user_role` (`role_id`,`user_id`),
  KEY `FK_user_user_role` (`user_id`),
  CONSTRAINT `FK_role_user_role` FOREIGN KEY (`role_id`) REFERENCES `t_role` (`id`),
  CONSTRAINT `FK_user_user_role` FOREIGN KEY (`user_id`) REFERENCES `t_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user_role
-- ----------------------------
INSERT INTO `t_user_role` VALUES ('1', '1', '1');
