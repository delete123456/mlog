package org.mspring.mlog.entity;

import org.mspring.nbee.orm.annotation.Col;
import org.mspring.nbee.orm.annotation.Id;
import org.mspring.nbee.orm.annotation.Mappable;
import org.mspring.nbee.orm.entity.BaseEntity;

/**
 * 
 * @author Gao Youbo
 * @since 2015-01-04 12:48:03
 */
@Mappable
public class Config extends BaseEntity {

    @Id
    @Col(length = 19)
    private Long id;
    @Col(nullable = false, length = 32)
    private String groupName;
    @Col(nullable = false, length = 64)
    private String name;
    @Col(nullable = false, length = 65535)
    private String value;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGroupName() {
        return this.groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
