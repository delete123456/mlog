package org.mspring.mlog.entity.em;

/**
 * 评论位
 *
 * @author Gao Youbo
 * @since 2015-01-04 17:27
 */
public class CommentPlace {
    /**
     * 文章
     */
    public static final String ARTICLE = "article";
}
