package org.mspring.mlog.entity;

import org.mspring.nbee.orm.annotation.Col;
import org.mspring.nbee.orm.annotation.Id;
import org.mspring.nbee.orm.annotation.Mappable;
import org.mspring.nbee.orm.entity.BaseEntity;

/**
 * 
 * @author Gao Youbo
 * @since 2014-12-31 17:52:14
 */
@Mappable
public class RoleResource extends BaseEntity {

    @Id
    @Col(length = 19)
    private Long id;
    @Col(nullable = false, length = 19)
    private long roleId;
    @Col(nullable = false, length = 19)
    private long resourceId;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getRoleId() {
        return this.roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }

    public long getResourceId() {
        return this.resourceId;
    }

    public void setResourceId(long resourceId) {
        this.resourceId = resourceId;
    }

}
