package org.mspring.mlog.entity.em;

/**
 * @author Gao Youbo
 * @since 2015-01-01 14:31
 */
public class NodeType {
    /**
     * 树形菜单文件夹
     */
    public static String TREE_FOLDER = "tree_folder";
    /**
     * 树形菜单节点
     */
    public static String TREE_ITEM = "tree_item";
    /**
     * tab
     */
    public static String TAB = "tab";
}
