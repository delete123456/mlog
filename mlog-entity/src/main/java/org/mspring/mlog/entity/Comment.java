package org.mspring.mlog.entity;

import org.mspring.nbee.orm.annotation.Col;
import org.mspring.nbee.orm.annotation.Id;
import org.mspring.nbee.orm.annotation.Mappable;
import org.mspring.nbee.orm.entity.BaseEntity;

import java.util.Date;

/**
 * @author Gao Youbo
 * @since 2015-01-03 14:49:10
 */
@Mappable
public class Comment extends BaseEntity {

    @Id
    @Col(length = 19)
    private Long id;
    @Col(nullable = false, length = 32)
    private String place;
    @Col(nullable = false, length = 19)
    private long topicId;
    @Col(length = 10)
    private Integer status;
    @Col(nullable = false, length = 65535)
    private String content;
    @Col(nullable = false, length = 32)
    private String authorName;
    @Col(length = 1024)
    private String authorAvatar;
    @Col(length = 1024)
    private String authorEmail;
    @Col(length = 512)
    private String authorUrl;
    @Col(length = 32)
    private String ip;
    @Col(length = 1024)
    private String renferer;
    @Col(nullable = false, length = 19)
    private Date createTime;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPlace() {
        return this.place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public long getTopicId() {
        return this.topicId;
    }

    public void setTopicId(long topicId) {
        this.topicId = topicId;
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAuthorName() {
        return this.authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorAvatar() {
        return this.authorAvatar;
    }

    public void setAuthorAvatar(String authorAvatar) {
        this.authorAvatar = authorAvatar;
    }

    public String getAuthorEmail() {
        return this.authorEmail;
    }

    public void setAuthorEmail(String authorEmail) {
        this.authorEmail = authorEmail;
    }

    public String getAuthorUrl() {
        return this.authorUrl;
    }

    public void setAuthorUrl(String authorUrl) {
        this.authorUrl = authorUrl;
    }

    public String getIp() {
        return this.ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getRenferer() {
        return this.renferer;
    }

    public void setRenferer(String renferer) {
        this.renferer = renferer;
    }

    public Date getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

}
