package org.mspring.mlog.entity.em;

/**
 * 评论状态
 *
 * @author Gao Youbo
 * @since 2015-01-03 14:49
 */
public class CommentStatus {
    /**
     * 正常状态
     */
    public static final int NORMAL = 0;
    /**
     * 删除状态
     */
    public static final int DELEtED = 1;
    /**
     * 垃圾
     */
    public static final int SPAM = 2;
}
