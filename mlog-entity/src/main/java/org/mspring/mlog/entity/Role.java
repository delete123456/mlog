package org.mspring.mlog.entity;

import org.mspring.nbee.orm.annotation.Col;
import org.mspring.nbee.orm.annotation.Id;
import org.mspring.nbee.orm.annotation.Mappable;
import org.mspring.nbee.orm.entity.BaseEntity;

/**
 * 
 * @author Gao Youbo
 * @since 2014-12-31 17:52:14
 */
@Mappable
public class Role extends BaseEntity {

    @Id
    @Col(length = 19)
    private Long id;
    @Col(nullable = false, length = 32)
    private String name;
    @Col(nullable = false, length = 1)
    private boolean enabled;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

}
