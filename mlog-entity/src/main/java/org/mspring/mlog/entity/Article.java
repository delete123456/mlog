package org.mspring.mlog.entity;

import org.mspring.nbee.orm.annotation.Col;
import org.mspring.nbee.orm.annotation.Id;
import org.mspring.nbee.orm.annotation.Mappable;
import org.mspring.nbee.orm.entity.BaseEntity;

import java.util.Date;

/**
 * 
 * @author Gao Youbo
 * @since 2014-12-31 17:52:14
 */
@Mappable
public class Article extends BaseEntity {

    @Id
    @Col(length = 19)
    private Long id;
    @Col(nullable = false, length = 19)
    private long category;
    @Col(nullable = false, length = 1024)
    private String title;
    @Col(nullable = false, length = 2147483647)
    private String content;
    @Col(length = 65535)
    private String imageList;
    @Col(nullable = false, length = 10)
    private int status;
    @Col(nullable = false, length = 19)
    private Date createTime;
    @Col(nullable = false, length = 19)
    private long createUser;
    @Col(nullable = false, length = 32)
    private String createUserName;
    @Col(nullable = false, length = 19)
    private Date updateTime;
    @Col(nullable = false, length = 19)
    private long updateUser;
    @Col(nullable = false, length = 32)
    private String updateUserName;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getCategory() {
        return this.category;
    }

    public void setCategory(long category) {
        this.category = category;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImageList() {
        return this.imageList;
    }

    public void setImageList(String imageList) {
        this.imageList = imageList;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public long getCreateUser() {
        return this.createUser;
    }

    public void setCreateUser(long createUser) {
        this.createUser = createUser;
    }

    public String getCreateUserName() {
        return this.createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public Date getUpdateTime() {
        return this.updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public long getUpdateUser() {
        return this.updateUser;
    }

    public void setUpdateUser(long updateUser) {
        this.updateUser = updateUser;
    }

    public String getUpdateUserName() {
        return this.updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

}
