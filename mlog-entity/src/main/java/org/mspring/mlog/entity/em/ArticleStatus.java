package org.mspring.mlog.entity.em;

/**
 * 文章状态
 *
 * @author Gao Youbo
 * @since 2015-01-01 15:11
 */
public class ArticleStatus {
    /**
     * 正常状态
     */
    public static final int NORMAL = 0;
    /**
     * 删除状态
     */
    public static final int DELEtED = 1;
}
