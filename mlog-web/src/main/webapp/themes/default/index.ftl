<#include "header.ftl"/>
<div class="am-g am-g-fixed blog-g-fixed">
    <div class="am-u-md-9">
    <#include "article-list.ftl"/>
    </div>

    <div class="am-u-md-3 blog-sidebar">
    <#include "sidebar.ftl" />
    </div>
</div>
<#include "footer.ftl" />