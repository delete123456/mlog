<#if (articleResult?exists && articleResult.data?exists && articleResult.data?size > 0)>
    <#list articleResult.data as article>
    <article class="blog-main">
        <h3 class="am-article-title blog-title">
            <a href="#">${article.title!""}</a>
        </h3>
        <h4 class="am-article-meta blog-meta">
            by <a href="">${article.createUserName}</a> posted
            on ${article.createTime?datetime}
            <#if article.category?exists>
                under <a href="#">${article.category.name!""}</a>
            </#if>
        </h4>

        <div class="am-g blog-content">
            <#if (article.imageList?exists && article.imageList?size > 0)>
                <div class="am-u-lg-7">
                    <@article_summary content=article.content/>
                </div>
                <div class="am-u-lg-5">
                    <p><img src="${article.imageList[0].url}"></p>
                </div>
            <#else>
                <div class="am-u-lg-12">
                    <@article_summary content=article.content/>
                </div>
            </#if>
        </div>
    </article>

    <hr class="am-article-divider blog-hr">
    </#list>

<ul class="am-pagination blog-pagination">
    <#if (articleResult.pager.page > 1)>
        <li class="am-pagination-prev"><a href="${base}/?page=${articleResult.pager.page - 1}">&laquo; 上一页</a></li>
    </#if>
    <#if (articleResult.pager.page < articleResult.totalPage)>
        <li class="am-pagination-next"><a href="${base}/?page=${articleResult.pager.page + 1}">下一页 &raquo;</a></li>
    </#if>
</ul>
</#if>