<#include "inc/header.ftl" />
<div class="admin-content">
    <table class="infotable">
        <tr>
            <td colspan="2" class="partition">联系方式</td>
        </tr>
        <tr>
            <td class="label">QQ:</td>
            <td class="field">330721072</td>
        </tr>
        <tr>
            <td class="label">QQ群:</td>
            <td class="field">100231009</td>
        </tr>
        <tr>
            <td class="label">邮箱:</td>
            <td class="field"><a href="mailto:gaoyoubo@mspring.org">gaoyoubo@mspring.org</a></td>
        </tr>
        <tr>
            <td class="label">网址:</td>
            <td class="field"><a href="http://www.mspring.org" target="_blank">http://www.mspring.org</a></td>
        </tr>
    </table>
</div>
<#include "inc/footer.ftl" />