<#include "../inc/header.ftl" />
<div class="admin-content">
    <ui id="error" class="message error" style="display:none;"></ui>
    <form id="baseConfigForm" name="baseConfigForm" method="post" action="${base}/admin/config/base/do">
        <table class="formtable">
            <tr>
                <td colspan="3" class="partition">通用配置</td>
            </tr>
            <tr>
                <td class="label" style="width: 100px;">网站名称</td>
                <td class="field">
                    <input type="text" class="textinput" name="config.sitename"/>
                </td>
                <td class="message">
                    描述
                </td>
            </tr>
            <tr>
                <td class="label" style="width: 100px;">网站地址</td>
                <td class="field">
                    <input type="text" class="textinput" name="config.siteurl"/>
                </td>
                <td class="message">
                    描述
                </td>
            </tr>
            <tr>
                <td colspan="3" class="toolbar">
                    <input class="btn btn-default" type="submit" value="提交表单"/>
                </td>
            </tr>
        </table>
    </form>
</div>
<#include "../inc/footer.ftl" />