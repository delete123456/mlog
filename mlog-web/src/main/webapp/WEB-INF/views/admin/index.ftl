<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>M-LOG</title>
    <link rel="stylesheet" href="${base}/script/easy-ui/themes/bootstrap/easyui.css">
    <script type="text/javascript" src="${base}/admin/script/variable"></script>
    <script type="text/javascript" src="${base}/script/jquery.js"></script>
    <script type="text/javascript" src="${base}/script/easy-ui/jquery.easyui.min.js"></script>
    <style type="text/css">
        * {
            font-size: 14px;
        }

        html, body {
            margin: 0;
            padding: 0;
        }

        body {
            font-family: 微软雅黑, verdana, helvetica, arial, sans-serif;
            text-align: center;
        }

        #header {
            text-align: center;
            direction: ltr;
        }

        #topmenu {
            text-align: right;
        }

        #topmenu a {
            display: inline-block;
            padding: 1px 3px;
            text-decoration: none;
        }

        #topmenu a:hover {
            text-decoration: underline;
        }

    </style>
</head>
<body id="main-layout" class="easyui-layout">
<div region="north" id="header" border="false" split="false" style="height:65px;overflow: visible;">
    <table cellpadding="0" cellspacing="0" style="width:100%;">
        <tr>
            <td rowspan="2" style="width:20px;">
            </td>
            <td style="height:52px;">
                <div style="font-size:22px;font-weight:bold;">
                    <a href="javascript:void(0);"
                       style="color:#182646;font-size:22px;font-weight:bold;text-decoration:none">M-LOG</a>
                </div>
                <div>
                    <a href="javascript:void(0);" style="color:#182646;text-decoration:none">轻.快</a>
                </div>
            </td>
            <td style="padding-right:5px;text-align:right;vertical-align:bottom;">
                <div id="topmenu">
                    <a href="javascript:void(0);">Home</a>
                    <a href="javascript:void(0);">Demo</a>
                    <a href="javascript:void(0);">Tutorial</a>
                    <a href="javascript:void(0);">Documentation</a>
                    <a href="javascript:void(0);">Download</a>
                    <a href="javascript:void(0);">Extension</a>
                    <a href="javascript:void(0);">Contact</a>
                    <a href="javascript:void(0);">Forum</a>
                </div>
            </td>
        </tr>
    </table>
</div>

<div region="west" title="导航" split="true" style="width:300px">
<@m.widget path="/admin/nav/leftMenu"/>
</div>
<div region="center">
    <div id="main-tabs" class="easyui-tabs" border="false" fit="true" lineHeight="5"></div>
</div>
<div region="south" border="true" style="text-align:center;height:30px;">
    <div>Copyright © 2010-2014 www.mspring.org</div>
</div>
</body>
<script type="text/javascript" src="${base}/script/admin/admin.index.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        window.maintab.addItem(1,
                '关于',
                '<iframe scrolling="auto" frameborder="0" src="${base}/admin/about" style="width:100%;height:100%;border:none;"></iframe>',
                false
        );
    });
</script>
</html>