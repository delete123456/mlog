<link rel="stylesheet" type="text/css" href="${base}/script/ztree/jquery.ztree.css">
<script type="text/javascript" src="${base}/script/ztree/jquery.ztree.js"></script>
<script type="text/javascript">
    var setting = {
        view: {
            showLine: true,
            selectedMulti: false,
            dblClickExpand: false
        },
        data: {
            simpleData: {
                enable: true
            }
        },
        callback: {
            onClick: onClick
        }
    };

    var zNodes = [
    <#if items?exists>
        <#list items as item>
            <#if item_has_next>
                {
                    id:${item.id},
                    pId:${item.parent?default("0")},
                    name: '${item.name?default("")}',
                    type: '${item.type}',
                    url: '<#if item.type = 'tree_item'>${base}/admin/redirect?id=${item.id}<#else>javascript:void(0);</#if>',
                    open:${item.open?string("true","false")}
                },
            <#else>
                {
                    id:${item.id},
                    pId:${item.parent?default("0")},
                    name: '${item.name?default("")}',
                    type: '${item.type}',
                    url: '<#if item.type = 'tree_item'>${base}/admin/redirect?id=${item.id}<#else>javascript:void(0);</#if>',
                    open:${item.open?string("true","false")}
                }
            </#if>
        </#list>
    </#if>
    ];

    function onClick(e, treeId, treeNode) {
        var zTree = $.fn.zTree.getZTreeObj("treeMenu");
        zTree.expandNode(treeNode);

        if (treeNode.type === 'tree_item') {
            var content = '<iframe scrolling="auto" frameborder="0" src="' + window.variables.base + treeNode.url + '" style="width:100%;height:100%;border:none;"></iframe>';
            window.maintab.addItem(treeNode.id, treeNode.name, content, true);
        }
        e.preventDefault();
    }

    $(document).ready(function () {
        $.fn.zTree.init($("#treeMenu"), setting, zNodes);
    });
</script>

<div class="treeMenuDiv">
    <ul id="treeMenu" class="ztree"></ul>
</div>