<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="${base}/script/easy-ui/themes/metro-gray/easyui.css">
    <script type="text/javascript" src="${base}/admin/script/variable"></script>
    <script type="text/javascript" src="${base}/script/jquery.js"></script>
    <script type="text/javascript" src="${base}/script/easy-ui/jquery.easyui.min.js"></script>
</head>
<style type="text/css">
    .frame-body {
        overflow: hidden;
        margin: 0px;
        padding: 0px;
    }

    .frame-panel {
        width: 100%;
        padding: 0px;
        margin: 0px;
        border: none;
    }

    .frame-tabs {
        width: 100%;
        position: fixed;
        bottom: 0;
        left: 0;
    }
</style>
<body class="frame-body">
<#assign openId = 0/>
<#assign manyTab = (tabs?size > 1)/>
<#if openTab?exists>
    <#assign openId = openTab.id />
</#if>
<div id="frame-panel" class="frame-panel">
    <iframe id="main-frame" frameborder="0" style="margin:0px; padding:0px; width:100%;height:100%;border:none;"
            src="${base}${openTab.url}"></iframe>
</div>
<#if (tabs?size > 0)>
    <#if tabs?size == 1>
        <#assign tab=tabs[0]/>
    <#else>
    <div class="tabs-header tabs-header-noborder tabs-header-bottom frame-tabs">
        <div class="tabs-wrap">
            <ul class="tabs">
                <#list tabs as tab>
                    <#assign open = false />
                    <#if openId?number == 0 && tab_index == 0>
                        <#assign open = true />
                        <#assign openId = tab.id />
                    <#elseif openId == tab.id>
                        <#assign open = true />
                    </#if>
                    <li id="tab-item-${tab.id}" data-src="${base}${tab.url}" data-id="${tab.id}"
                        data-clickable="${tab.clickable?string}"
                        <#if open>class="tabs-selected"
                        <#else>class="tabs-inner"</#if>>
                        <a href="javascript:void(0);" class="tabs-inner">
                            <span class="tabs-title">${tab.name}</span>
                            <span class="tabs-icon"></span>
                        </a>
                    </li>
                </#list>
            </ul>
        </div>
    </div>
    </#if>
</#if>
<script type="text/javascript">
    var space = <#if manyTab>30<#else>0</#if>;
    $('#frame-panel').height($(window).height() - space);
    $(window).resize(function () {
        $('#frame-panel').height($(window).height() - space);
    });

    $('ul.tabs li').click(function () {
        if ($(this).attr('class') == 'tabs-selected' || $(this).attr('data-clickable') == 'false') {
            return;
        }
        var src = $(this).attr('data-src');
        $('#main-frame').attr('src', src);
        $('ul.tabs li').each(function () {
            $(this).attr('class', 'tabs-inner');
        });
        $(this).attr('class', 'tabs-selected');
    });
</script>
</body>
</html>