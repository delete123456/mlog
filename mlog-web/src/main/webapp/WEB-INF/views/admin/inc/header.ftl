<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <link rel="stylesheet" href="${base}/style/admin.css"/>

    <script type="text/javascript" src="${base}/admin/script/variable"></script>
    <#--
    <script type="text/javascript" src="http://libs.baidu.com/jquery/2.0.3/jquery.min.js"></script>
    <link rel="stylesheet" href="http://libs.baidu.com/jqueryui/1.10.2/themes/smoothness/jquery-ui.css"/>
    <script type="text/javascript" src="http://libs.baidu.com/jqueryui/1.10.2/jquery-ui.min.js"></script>
    -->
    <script type="text/javascript" src="${base}/script/jquery.js"></script>
    <link rel="stylesheet" href="${base}/script/jquery-ui/themes/base/jquery.ui.all.css" />
    <script type="text/javascript" src="${base}/script/jquery-ui/jquery-ui-1.10.4.min.js"></script>

    <script type="text/javascript" src="${base}/script/jquery-validation/jquery.validate.min.js"></script>
    <script type="text/javascript" src="${base}/script/jquery-validation/localization/messages_zh.min.js"></script>

    <script type="text/javascript" src="${base}/script/ajax.js"></script>
    <script type="text/javascript" src="${base}/script/admin/admin.js"></script>
</head>
<body>
<input id="__parentItemId_" type="hidden" value="${_parent_item!""}"/>
<input id="__treeItemId_" type="hidden" value="${_item!""}"/>
<script type="text/javascript">
    $(document).ready(function () {
        if ($('#__treeItemId_') && $('#__treeItemId_').val()) {
            var item = $('#__treeItemId_').val();
            $('ul.tabs li', window.parent.document).each(function () {
                $(this).attr('class', 'tabs-inner');
                if ($(this).attr('data-id') == item) {
                    $(this).attr('class', 'tabs-selected');
                }
            });
        }
    });
</script>