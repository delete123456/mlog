<#macro pager result formId>
<div class="pagger">
    <span>共${result.total}条</span>
    <span>第${result.pager.page}/${result.totalPage}页</span>

    <#if (result.pager.page > 1)>
        <span><a href="javascript:changePage('${formId}', 1);">首页</a></span>
        <span><a href="javascript:changePage('${formId}', ${result.pager.page - 1});">上一页</a></span>
    <#else>
        <span class="disabled">首页</span>
        <span class="disabled">上一页</span>
    </#if>

    <#if (result.pager.page < result.totalPage)>
        <span><a href="javascript:changePage('${formId}', ${result.pager.page + 1});">下一页</a></span>
        <span><a href="javascript:changePage('${formId}', ${result.totalPage});">末页</a></span>
    <#else>
        <span class="disabled">下一页</span>
        <span class="disabled">末页</span>
    </#if>
    <span>跳转到</span>
    <input type="text" class="textinput" style="width:20px;" name="goPageNumber"/>
    <span><a href="javascript:changePageCustom('${formId}');">GO</a></span>
</div>
<script type="text/javascript">
    function changePage(formId, pageNumber) {
        if (pageNumber < 1) {
            pageNumber = 1;
        }
        var pageNoObj = document.getElementById("page");
        if (pageNoObj) {
            pageNoObj.value = pageNumber;
        }
        document.getElementById(formId).submit();
    }
    function changePageCustom(formId) {
        var pageNumbers = document.getElementsByName('goPageNumber');
        if (!pageNumbers) {
            changePage(formId, 1);
        }
        var pageNumber = 1;
        for (var i = 0; i < pageNumbers.length; i++) {
            if (pageNumber < pageNumbers[i].value) {
                pageNumber = pageNumbers[i].value;
            }
        }
        changePage(formId, pageNumber);
    }
</script>
</#macro>