<#include "../inc/header.ftl" />
<#include "../inc/macro.ftl" />
<!-- content start -->
<div class="admin-content">
    <form id="commentForm" name="commentForm" action="${base}/admin/comment/list" method="POST">
        <input type="hidden" id="page" name="page" value="${result.pager.page}"/>
        <table class="stable">
            <tr>
                <td class="label">内容：</td>
                <td class="field">
                    <input type="text" class="textinput" name="content" value="${queryparam_name!""}"/>
                </td>
                <td class="field"><input type="submit" class="btn btn-default" value="搜索"/></td>
            </tr>
        </table>
        <table class="dtable">
            <tr>
                <th>编号</th>
                <th>名称</th>
                <th>修改人</th>
                <th>修改日期</th>
                <th>操作</th>
            </tr>
        <#if (result?exists && result.data?exists && result.data?size > 0)>
            <#list result.data as item>
                <tr>
                    <td>${item.id}</td>
                    <td>${item.name}</td>
                    <td>${item.updateUserName}</td>
                    <td>${item.updateTime?datetime}</td>
                    <td>
                        <a href="javascript:admin.comment.delete(${item.id})">删除</a> | <a
                            href="${base}/admin/comment/edit?id=${item.id}">修改</a>
                    </td>
                </tr>
            </#list>
        </#if>
        </table>
        <table class="tool-table">
            <tr>
                <td>
                    <input type="button" class="btn btn-danger" value=" 删除选中 " onclick=""/>
                </td>
                <td>
                <@pager result "commentForm"/>
                </td>
            </tr>
        </table>

    </form>
</div>
<!-- content end -->
<#include "../inc/footer.ftl" />