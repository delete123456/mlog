<#include "../inc/header.ftl" />
<div class="admin-content">
    <ui id="error" class="message error" style="display:none;"></ui>
    <form id="commentForm" name="commentForm" method="post" action="${base}/admin/comment/edit/do">
        <table class="formtable">
            <tr>
                <td class="label">编号</td>
                <td class="field"><input type="text" class="textinput" readonly name="id" value="${comment.id}"/></td>
            </tr>

            <tr>
                <td class="label">分类名称</td>
                <td class="field"><input type="text" class="textinput" name="name" value="${comment.name}"/></td>
            </tr>
            <tr>
                <td class="label">描述</td>
                <td class="field">
                    <textarea class="textinput textarea" name="description">${comment.description!""}</textarea>
                </td>
            </tr>
            <tr>
                <td class="label">创建时间</td>
                <td class="field"><input type="text" class="textinput" disabled="disabled" name="createTime"
                                         value="${comment.createTime?datetime}"/></td>
            </tr>
            <tr>
                <td colspan="2" class="toolbar">
                    <input class="btn btn-default" type="submit" value="提交表单"/>
                </td>
            </tr>
        </table>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        admin.comment.validateForm();
    });
</script>
<#include "../inc/footer.ftl" />