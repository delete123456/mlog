<#include "../inc/header.ftl" />
<div class="admin-content">
    <ui id="error" class="message error" style="display:none;"></ui>
    <form id="articleForm" name="articleForm" method="post" action="${base}/admin/article/edit/do">
        <input type="hidden" name="id" value="${article.id}"/>
        <input type="hidden" name="imageList" id="imageList"
               value="<#if article.imageList?has_content>${article.imageList?xhtml}</#if>"/>
        <table class="formtable">
            <tr>
                <td class="label" style="width: 100px;">文章标题</td>
                <td class="field"><input type="text" class="textinput" name="title" value="${article.title}"/></td>
                <td class="label" style="width: 100px;">分类</td>
                <td class="field">
                    <select class="select" id="category" name="category">
                        <option value="">请选择分类</option>
                    <#if categoryList?exists>
                        <#list categoryList as category>
                            <option value="${category.id}"
                                    <#if category.id == article.category>selected="selected" </#if>>${category.name}</option>
                        </#list>
                    </#if>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="label" style="width: 100px;">文章标签</td>
                <td class="field" colspan="3">
                <#assign tags = "" />
                <#list tagList as tag>
                    <#assign tags = tags + tag.name + "," />
                </#list>
                    <input type="text" class="textinput" name="tags" id="tags" value="${tags!""}"/>
                </td>
            </tr>
            <tr>
                <td class="label" style="width: 100px;">正文</td>
                <td class="field" colspan="3">
                    <textarea id="content" name="content"
                              style="width:99%;height:380px;">${article.content!""}</textarea>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                <#include "article-image.ftl" />
                </td>
            </tr>
            <tr>
                <td colspan="2" class="toolbar">
                    <input class="btn btn-default" type="submit" value="提交表单"/>
                </td>
            </tr>
        </table>
    </form>
</div>
<script type="text/javascript" src="${base}/script/kindeditor/kindeditor-all-min.js"></script>
<script type="text/javascript" src="${base}/script/jquery-tagsinput/jquery.tagsinput.min.js"></script>
<link rel="stylesheet" href="${base}/script/jquery-tagsinput/jquery.tagsinput.css"/>

<script type="text/javascript">
    $(document).ready(function () {
        admin.editor('content');
        admin.article.validateForm();
        $('#tags').tagsInput({
            width: '98.5%',
            height: '28px',
            defaultText: '添加标签',
            autocomplete_url: '${base}/admin/tag/autocomplete',
            autocomplete: {
                minLength: 1,
                source: function (request, response) {
                    var term = request.term;
                    httpPost('${base}/admin/tag/autocomplete', {name: extractLast(request.term)}, function (data) {
                        response($.ui.autocomplete.filter(
                                $.map(data, function (item) {
                                    return item.name
                                }), extractLast(request.term)));
                    });
                }
            }
        });

        function split(val) {
            return val.split(/,\s*/);
        }

        function extractLast(term) {
            return split(term).pop();
        }
    });
</script>
<#include "../inc/footer.ftl" />