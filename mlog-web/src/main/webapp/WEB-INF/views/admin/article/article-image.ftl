<link rel="stylesheet" type="text/css" href="${base}/script/uploadify/uploadify.css">
<style type="text/css">
    .article-image {
        border: solid #DFDFDF 1px;
    }

    .article-image a {
        color: #999999;
        text-decoration: none;
        outline: none;
    }

    .article-image a:hover {
        color: #333;
        text-decoration: none
    }

    .article-image .upload-header {
        border-bottom: solid #DFDFDF 1px;
        background-color: #F1F1F1;
        margin-top: 0px;
        height: 20px;
    }

    .article-image .upload-header span {
        margin: 0px 3px 0px 3px;
        list-style-type: none;
        display: block;
        float: left;
        zoom: 1;
    }

    .article-image .upload-header span.curtab {
        background-color: #FFF;
        border-left: 1px solid #F0F0F0;
        border-right: 1px solid #F0F0F0;
        border-top: 1px solid #F0F0F0;
        padding: 0px 5px 0px 5px;
    }

    .article-image .upload-body {
        margin: 0px 1px 5px 1px;
    }

    .article-image .upload-body .upload-div {
        margin: 5px 0px;
    }

    .article-image .images-body {
        padding: 10px;
    }

    .article-image .images-body .image-wraper {
        display: inline-block;
        padding: 2px;
        margin: 2px;
        background-color: #ffffff;
        border: 1px solid #dddddd;
        border-radius: 0;
        transition: all 0.2s ease-in-out;
        -webkit-transition: all 0.2s ease-in-out;
        /*width: 140px;*/
        /*height: 140px;*/
        position: relative;
        /*display: table-cell;*/
        display: inline-block;
        text-align: center;
        vertical-align: middle;
    }

    .article-image .images-body .img {
        /*max-width: 140px;*/
        /*max-height: 140px;*/
        width: 140px;
        height: 140px;
        float: left;
        display: table-cell;
    }

    .article-image .images-body .img.radius {
        border-radius: 2px;
    }

    .article-image .images-body .image-bottom a {
        font-size: 12px;
        font-weight: normal;
        color: #999999;
        text-decoration: none;
        outline: none;
    }

    .article-image .images-body .image-bottom a:hover {
        color: #333;
        text-decoration: none;
    }
</style>

<div class="article-image">
    <div class="upload-header">
        <span><a href="javascript:;">图片列表</a></span>
    <#--
    <span class="curtab"><a href="javascript:;">批量上传</a></span>
    <span><a href="javascript:showAttachs();">图片列表</a></span>
    -->
    </div>
    <div class="images-body" id="images-body">
    </div>
    <div class="upload-body">
        <div class="upload-div">
            <form enctype="multipart/form-data" method="post">
                <input type="file" name="filedata" multiple="true" id="article-image-file">
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="${base}/script/uploadify/jquery.uploadify.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        recoverImageList();
        $("#article-image-file").uploadify({
            //multi: true,
            //queueID: 'custom-queue',
            swf: '${base}/script/uploadify/uploadify.swf',
            uploader: '${base}/admin/article/edit/upload',
            buttonText: '选择文件',
            progressData: 'speed',
            fileTypeExts: '*.jpg;*.gif;*.png;*.jpeg;*.bmp;',
            fileSizeLimit: 20971520,
            checkExisting: false,
            auto: true,
            queueSizeLimit: 30,
            removeCompleted: true,
            fileObjName: 'image',
            onUploadSuccess: function (file, data, response) {
                var ret = JSON.parse(data);
                if (ret.success) {
                    if (!existsImage(ret.data.key)) {
                        appendImage(ret.data);
                    }
                    else {
                        console.log('图片已经存在');
                    }
                } else {
                    alert(ret.message);
                }
            },
            onQueueComplete: function () {
            }
        });
    });

    /**
     * 追加图片
     * @param data
     */
    function appendImage(data) {
        var html = [
            '<div class="image-wraper" id="image-wraper-' + data.key + '"><a style="text-align: center;vertical-align: middle;">',
            '<a style="text-align: center;vertical-align: middle;">',
            '<img class="img radius" src="' + data.url + '" data-item-key="' + data.key + '" data-item-hash="' + data.hash + '" />',
            '</a>',
            '<div class="image-bottom">',
            '<a href="javascript:removeImage(\'' + data.key + '\')">删除</a>&nbsp;',
            '<a href="javascript:insertImage(\'' + data.url + '\', \'' + data.key + '\')">插入</a>',
            '</div>',
            '</div>'
        ];
        $('#images-body').append(html.join(''))
        setImageListValue();
    }

    /**
     * 删除图片
     * @param key
     */
    function removeImage(key) {
        $('#image-wraper-' + key).remove();
        setImageListValue();
    }

    /**
     * 图片是否存在
     * @param key
     */
    function existsImage(key) {
        var exists = false;
        $('#images-body .image-wraper img').each(function () {
            var thisKey = $(this).attr('data-item-key');
            if (thisKey == key) {
                exists = true;
                return;
            }
        });
        return exists;
    }

    /**
     * 获取已经上传的图片列表
     * @returns {Array}
     */
    function getImageList() {
        var array = new Array();
        $('#images-body .image-wraper img').each(function () {
            var url = $(this).attr('src');
            var key = $(this).attr('data-item-key');
            var hash = $(this).attr('data-item-hash');
            array.push({
                'url': url,
                'key': key,
                'hash': hash
            });
        });
        return array;
    }


    /**
     * 根据imageList的值恢复图像列表的显示
     */
    function recoverImageList() {
        var imageListStr = $('#imageList').val();
        if (!imageListStr) {
            return;
        }
        var imageList = JSON.parse(imageListStr);
        if (imageList.length > 0) {
            for (var i = 0; i < imageList.length; i++) {
                appendImage(imageList[i]);
            }
        }
    }

    /**
     * 设置imageList表单值
     */
    function setImageListValue() {
        var array = getImageList();
        var imageList = JSON.stringify(array);
        $('#imageList').val(imageList);
    }

    /**
     * 将图片插入编辑器
     *
     * @param url
     * @param key
     */
    function insertImage(url, key) {
        UE.getEditor('content').execCommand('insertHtml', '<img src="' + url + '" class="article-image" data-item-key="' + key + '"/>');
    }
</script>