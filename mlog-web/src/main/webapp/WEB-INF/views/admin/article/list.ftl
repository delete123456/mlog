<#include "../inc/header.ftl" />
<#include "../inc/macro.ftl" />
<!-- content start -->
<div class="admin-content">
    <form id="articleForm" name="articleForm" action="${base}/admin/article/list" method="POST">
        <input type="hidden" id="page" name="page" value="${result.pager.page}"/>
        <table class="stable">
            <tr>
                <td class="label">分类：</td>
                <td class="field">
                    <select class="select" id="category" name="category">
                        <option value="">请选择分类</option>
                    <#if categoryList?exists>
                        <#list categoryList as category>
                            <option value="${category.id}"
                                    <#if queryparam_category?exists && queryparam_category?has_content && queryparam_category?number == category.id>selected="selected"</#if>>${category.name}</option>
                        </#list>
                    </#if>
                    </select>
                </td>
                <td class="label">标题：</td>
                <td class="field">
                    <input type="text" class="textinput" name="title" value="${queryparam_title!""}"/>
                </td>
                <td class="field"><input type="submit" class="btn btn-default" value="搜索"/></td>
            </tr>
        </table>
        <table class="dtable">
            <tr>
                <th>编号</th>
                <th>标题</th>
                <th>作者</th>
                <th>创建时间</th>
                <th>操作</th>
            </tr>
        <#if (result?exists && result.data?exists && result.data?size > 0)>
            <#list result.data as item>
                <tr>
                    <td>${item.id}</td>
                    <td>${item.title}</td>
                    <td>${item.createUserName}</td>
                    <td>${item.createTime?datetime}</td>
                    <td>
                        <a href="javascript:admin.article.delete(${item.id})">删除</a> | <a
                            href="${base}/admin/article/edit?id=${item.id}">修改</a>
                    </td>
                </tr>
            </#list>
        </#if>
        </table>
        <table class="tool-table">
            <tr>
                <td>
                    <input type="button" class="btn btn-danger" value=" 删除选中 " onclick=""/>
                </td>
                <td>
                <@pager result "articleForm"/>
                </td>
            </tr>
        </table>

    </form>
</div>
<!-- content end -->
<#include "../inc/footer.ftl" />