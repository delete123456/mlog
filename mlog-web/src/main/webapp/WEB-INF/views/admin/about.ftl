<#include "inc/header.ftl" />
<div class="admin-content">
    <table class="infotable">
        <tr>
            <td colspan="2" class="partition">关于M-LOG</td>
        </tr>
        <tr>
            <td class="label" style="width:80px;">应用名称:</td>
            <td class="field">${app.productName}</td>
        </tr>
        <tr>
            <td class="label">版本号:</td>
            <td class="field">${app.version}</td>
        </tr>
        <tr>
            <td class="label">LICENSE:</td>
            <td class="field"><a href="http://www.apache.org/licenses/LICENSE-2.0.html" target="_blank">${app.license}</a></td>
        </tr>
        <tr>
            <td class="label">官网地址:</td>
            <td class="field"><a href="http://www.mspring.org" target="_blank">${app.homePage}</a></td>
        </tr>
    </table>
    <br/>
    <table class="infotable">
        <tr>
            <td colspan="2" class="partition">系统状态</td>
        </tr>
        <tr>
            <td class="label" style="width:80px;">服务器名:</td>
            <td class="field">${serverName}(${remoteAddr})</td>
        </tr>
        <tr>
            <td class="label">内存消耗:</td>
            <td class="field">
            ${fUsedMemory/1024/1024}M / ${fTotalMemory/1024/1024}M
                <div style="width:200px; height:10px; background:#f00; font-size:1px">
                    <div style="float:right; background:#0f0; width:${fPercent*2}px; font-size:1px; height:10px"></div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="label" style="width:80px;">操作系统:</td>
            <td class="field">${os}</td>
        </tr>
        <tr>
            <td class="label" style="width:80px;">JDK版本:</td>
            <td class="field">${javaVersion}</td>
        </tr>
    </table>
</div>
<#include "inc/footer.ftl" />