(function ($) {
    window.mainlayout = $('#main-layout');
    window.maintab = $("#main-tabs");
    window.maintab.addItem = function (id, title, content, closable) {
        if (window.maintab.exists(title)) {
            window.maintab.select(title);
            return;
        }
        window.maintab.tabs('add', {
            id: id,
            title: title,
            content: content,
            closable: closable
        });
    };

    /**
     * @param title
     * @returns {number}
     */
    window.maintab.exists = function (title) {
        return window.maintab.tabs('exists', title);
    };

    window.maintab.select = function (title) {
        window.maintab.tabs('select', title);
    };

    window.maintab.selectById = function (id) {
        var tabs = window.maintab.tabs('tabs');
        $(tabs).each(function () {
            var options = this.panel('options');
            if (options.id = id) {
                window.maintab.select(options.title);
            }
        });
    };
})(jQuery);