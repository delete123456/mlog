(function ($) {
    window.admin = {};
    admin.UEditor = function (id) {
        UE.getEditor(id, {
            toolbars: [[
                'fullscreen', 'source', '|', 'undo', 'redo', '|',
                'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', 'cleardoc', '|',
                'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
                'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
                'directionalityltr', 'directionalityrtl', 'indent', '|',
                'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|', 'touppercase', 'tolowercase', '|',
                'link', 'unlink', 'anchor', '|', 'imagenone', 'imageleft', 'imageright', 'imagecenter', '|',
                'emotion', 'scrawl', 'music', 'map', 'gmap', 'insertframe', 'insertcode', 'webapp', 'pagebreak', 'template', 'background', '|',
                'horizontal', 'date', 'time', 'spechars', 'snapscreen', 'wordimage', '|',
                'inserttable', 'deletetable', 'insertparagraphbeforetable', 'insertrow', 'deleterow', 'insertcol', 'deletecol', 'mergecells', 'mergeright', 'mergedown', 'splittocells', 'splittorows', 'splittocols', 'charts', '|',
                'print', 'preview', 'searchreplace', 'drafts'
            ]]
        });
    };

    admin.editor = function (id) {
        KindEditor.create('#' + id);
    };

    admin.article = {};
    admin.category = {};
    admin.tag = {};


    ////////////////////////////////////////////////////////////
    //admin.article
    ////////////////////////////////////////////////////////////

    /**
     * 删除文章
     * @param id
     */
    admin.article.delete = function (id) {
        httpPost('/admin/article/delete/do', {'id': id}, function (data) {
            if (data == true) {
                $('#articleForm').submit();
            }
        });
    };


    /**
     * 文章分类表单验证
     */
    admin.article.validateForm = function () {
        $('#articleForm').validate({
            wrapper: 'li',
            errorLabelContainer: '#error',
            rules: {
                title: {
                    required: true,
                    maxlength: 50
                },
                category: {
                    required: true
                },
                content: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: '请填写文章标题',
                    maxlength: '分类名称最多不能超过{0}个字'
                },
                category: {
                    required: '请选择文章分类'
                },
                content: {
                    required: '文章内容不能为空'
                }
            }
        });
    }


    ////////////////////////////////////////////////////////////
    //admin.category
    ////////////////////////////////////////////////////////////

    /**
     * 删除分类
     *
     * @param id
     */
    admin.category.delete = function (id) {
        httpPost('/admin/category/delete/do', {'id': id}, function (data) {
            if (data == true) {
                $('#categoryForm').submit();
            }
        });
    };


    /**
     * 文章分类表单验证
     */
    admin.category.validateForm = function () {
        $('#categoryForm').validate({
            wrapper: 'li',
            errorLabelContainer: '#error',
            rules: {
                name: {
                    required: true,
                    maxlength: 12
                },
                description: {
                    maxlength: 500
                }
            },
            messages: {
                name: {
                    required: '请填写分类名称',
                    maxlength: '分类名称最多不能超过{0}个字'
                },
                description: {
                    maxlength: '描述最多不能超过{0}个字'
                }
            }
        });
    }


    ////////////////////////////////////////////////////////////
    //admin.tag
    ////////////////////////////////////////////////////////////

    /**
     * 删除标签
     *
     * @param id
     */
    admin.tag.delete = function (id) {
        httpPost('/admin/tag/delete/do', {'id': id}, function (data) {
            if (data == true) {
                $('#tagForm').submit();
            }
        });
    };


    /**
     * 标签表单验证
     */
    admin.tag.validateForm = function () {
        $('#tagForm').validate({
            wrapper: 'li',
            errorLabelContainer: '#error',
            rules: {
                name: {
                    required: true,
                    maxlength: 12
                },
                description: {
                    maxlength: 500
                }
            },
            messages: {
                name: {
                    required: '请填写标签名称',
                    maxlength: '标签名称最多不能超过{0}个字'
                },
                description: {
                    maxlength: '描述最多不能超过{0}个字'
                }
            }
        });
    }
})(jQuery);
