package org.mspring.mlog.dao.impl;

import org.mspring.mlog.dao.UserDao;
import org.mspring.mlog.entity.User;
import org.mspring.nbee.orm.dao.impl.BaseDao;
import org.mspring.nbee.orm.dao.sql.Sql;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Gao Youbo
 * @since 2014-09-10 11:51:15
 */
@Repository
public class UserDaoImpl extends BaseDao<User> implements UserDao {

    @Override
    public User findByUsername(String username) {
        Sql sql = new Sql("select * from t_user where username = ?", username);
        return findBySql(sql);
    }

}
