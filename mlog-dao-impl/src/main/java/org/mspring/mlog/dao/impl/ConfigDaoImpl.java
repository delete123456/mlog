package org.mspring.mlog.dao.impl;

import org.mspring.mlog.dao.ConfigDao;
import org.mspring.mlog.entity.Config;
import org.mspring.nbee.orm.dao.impl.BaseDao;
import org.mspring.nbee.orm.dao.sql.Sql;
import org.springframework.stereotype.Repository;

/**
 * @author Gao Youbo
 * @since 2015-01-03 18:08:01
 */
@Repository
public class ConfigDaoImpl extends BaseDao<Config> implements ConfigDao {

    @Override
    public Config findByName(String groupName, String name) {
        Sql sql = new Sql("select * from t_config where group_name = ? and name = ?", groupName, name);
        return super.findBySql(sql);
    }
}
