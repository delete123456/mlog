package org.mspring.mlog.dao.impl;

import org.mspring.mlog.dao.ArticleTagDao;
import org.mspring.mlog.entity.ArticleTag;
import org.mspring.nbee.orm.dao.impl.BaseDao;
import org.mspring.nbee.orm.dao.sql.Sql;
import org.springframework.stereotype.Repository;

/**
 * @author Gao Youbo
 * @since 2015-01-03 13:42:01
 */
@Repository
public class ArticleTagDaoImpl extends BaseDao<ArticleTag> implements ArticleTagDao {

    @Override
    public boolean deleteByArticle(long articleId) {
        Sql sql = new Sql("delete from t_article_tag where article_id = ?", articleId);
        return super.executeUpdate(sql) > 0;
    }
}
