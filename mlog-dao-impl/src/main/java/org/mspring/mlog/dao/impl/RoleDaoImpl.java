package org.mspring.mlog.dao.impl;

import java.util.List;
import org.mspring.mlog.dao.RoleDao;
import org.mspring.mlog.entity.Role;
import org.mspring.nbee.orm.dao.impl.BaseDao;
import org.mspring.nbee.orm.dao.sql.Sql;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Gao Youbo
 * @since 2014-09-10 11:51:15
 */
@Repository
public class RoleDaoImpl extends BaseDao<Role> implements RoleDao {

    @Override
    public List<Role> listByUser(long userId) {
        Sql sql = new Sql("select role.* from t_role role left join t_user_role user_role on role.id = user_role.role_id where user_role.user_id = ?", userId);
        return super.listBySql(sql);
    }

}
