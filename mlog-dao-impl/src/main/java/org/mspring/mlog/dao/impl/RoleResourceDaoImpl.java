package org.mspring.mlog.dao.impl;

import org.mspring.nbee.orm.dao.impl.BaseDao;
import org.springframework.stereotype.Repository;
import org.mspring.mlog.entity.RoleResource;
import org.mspring.mlog.dao.RoleResourceDao;

/**
 *
 * @author Gao Youbo
 * @since 2014-09-10 11:51:15
 */
@Repository
public class RoleResourceDaoImpl extends BaseDao<RoleResource> implements RoleResourceDao {

}
