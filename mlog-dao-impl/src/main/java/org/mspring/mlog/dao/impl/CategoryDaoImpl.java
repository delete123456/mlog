package org.mspring.mlog.dao.impl;

import org.mspring.nbee.orm.dao.impl.BaseDao;
import org.springframework.stereotype.Repository;
import org.mspring.mlog.entity.Category;
import org.mspring.mlog.dao.CategoryDao;

/**
 *
 * @author Gao Youbo
 * @since 2014-12-05 11:50:09
 */
@Repository
public class CategoryDaoImpl extends BaseDao<Category> implements CategoryDao {

}
