package org.mspring.mlog.dao.impl;

import org.mspring.mlog.dao.TagDao;
import org.mspring.mlog.entity.Tag;
import org.mspring.nbee.orm.dao.impl.BaseDao;
import org.mspring.nbee.orm.dao.sql.Sql;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Gao Youbo
 * @since 2015-01-02 15:49:57
 */
@Repository
public class TagDaoImpl extends BaseDao<Tag> implements TagDao {

    @Override
    public List<Tag> listByName(String name) {
        Sql sql = new Sql("select * from t_tag where name like ?", "%" + name + "%");
        return super.listBySql(sql);
    }

    @Override
    public List<Tag> listByArticle(long articleId) {
        Sql sql = new Sql("select t.* from t_tag t left join t_article_tag at on t.id = at.tag_id where at.article_id = ?", articleId);
        return super.listBySql(sql);
    }

    @Override
    public Tag findByName(String name) {
        Sql sql = new Sql("select * from t_tag where name = ?", name);
        return super.findBySql(sql);
    }
}
