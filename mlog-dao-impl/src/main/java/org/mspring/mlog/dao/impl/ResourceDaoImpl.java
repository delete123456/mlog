package org.mspring.mlog.dao.impl;

import java.util.List;
import org.mspring.mlog.dao.ResourceDao;
import org.mspring.mlog.entity.Resource;
import org.mspring.nbee.orm.dao.impl.BaseDao;
import org.mspring.nbee.orm.dao.sql.Sql;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Gao Youbo
 * @since 2014-09-10 11:51:15
 */
@Repository
public class ResourceDaoImpl extends BaseDao<Resource> implements ResourceDao {

    @Override
    public List<Resource> listByRole(long roleId) {
        Sql sql = new Sql("select res.* from t_resource res left join t_role_resource role_res on res.id = role_res.resource_id where role_res.role_id = ?", roleId);
        return super.listBySql(sql);
    }

}
