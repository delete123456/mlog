package org.mspring.mlog.dao.impl;

import org.mspring.nbee.orm.dao.impl.BaseDao;
import org.springframework.stereotype.Repository;
import org.mspring.mlog.entity.Article;
import org.mspring.mlog.dao.ArticleDao;

/**
 *
 * @author Gao Youbo
 * @since 2014-12-04 17:58:26
 */
@Repository
public class ArticleDaoImpl extends BaseDao<Article> implements ArticleDao {

}
