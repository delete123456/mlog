package org.mspring.mlog.dao.impl;

import org.mspring.nbee.orm.dao.impl.BaseDao;
import org.springframework.stereotype.Repository;
import org.mspring.mlog.entity.UserRole;
import org.mspring.mlog.dao.UserRoleDao;

/**
 *
 * @author Gao Youbo
 * @since 2014-09-10 11:51:15
 */
@Repository
public class UserRoleDaoImpl extends BaseDao<UserRole> implements UserRoleDao {

}
