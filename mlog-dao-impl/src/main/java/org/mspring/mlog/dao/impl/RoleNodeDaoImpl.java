package org.mspring.mlog.dao.impl;

import org.mspring.mlog.dao.RoleNodeDao;
import org.mspring.mlog.entity.RoleNode;
import org.mspring.nbee.orm.dao.impl.BaseDao;
import org.mspring.nbee.orm.dao.sql.Sql;
import org.springframework.stereotype.Repository;

/**
 * @author Gao Youbo
 * @since 2015-01-02 14:28
 */
@Repository
public class RoleNodeDaoImpl extends BaseDao<RoleNode> implements RoleNodeDao {
    @Override
    public boolean deleteAll() {
        Sql sql = new Sql("delete from t_role_node");
        return super.executeUpdate(sql) > 0;
    }
}
