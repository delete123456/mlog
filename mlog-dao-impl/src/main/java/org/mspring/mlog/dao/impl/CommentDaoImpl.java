package org.mspring.mlog.dao.impl;

import org.mspring.mlog.dao.CommentDao;
import org.mspring.mlog.entity.Comment;
import org.mspring.nbee.orm.dao.impl.BaseDao;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Gao Youbo
 * @since 2015-01-03 14:38:20
 */
@Repository
public class CommentDaoImpl extends BaseDao<Comment> implements CommentDao {

}
